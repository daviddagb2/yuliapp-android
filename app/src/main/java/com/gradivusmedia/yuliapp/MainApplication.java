package com.gradivusmedia.yuliapp;

import android.app.Application;
import android.content.Context;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.utils.Funciones;


public class MainApplication extends Application {

    private static Context context;

    @Override
    public void onCreate(){
        super.onCreate();
        context = getApplicationContext();
        initInjector();

    }

    private void initInjector(){
        DI.buildWithProdModules(this);
        //DI.get().inject(EventBus.getDefault());
    }

    public static Context getContext() {
        return context;
    }

}
