package com.gradivusmedia.yuliapp.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.print.PageRange;

public class MConfig {

    private static final String IDAPP = "1";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_NEW_JOB_RETRY_COUNT = "new_post_retry_count";
    private static final String KEY_API_URL = "http://apps.gradivusmedia.com/";
    private static final boolean ISDEBUG = true;
    private static String AP = "EAA694A81AD251B64C2E6D9D6FE6FF22";
    private static final String MOPUBID = "43cf8abe498242a89ebe7a14a3ccf1d0";
    private static final long placementID = 1540045671510L;


    public static final String WELCOME_TITLE_PAGINA1 = "welcome_title_1";
    public static final String WELCOME_TITLE_PAGINA2 = "welcome_title_2";
    public static final String WELCOME_TITLE_PAGINA3 = "welcome_title_3";
    public static final String WELCOME_COLOR_PAGINA1 = "welcome_message_color_1";
    public static final String WELCOME_COLOR_PAGINA2 = "welcome_message_color_2";
    public static final String WELCOME_COLOR_PAGINA3 = "welcome_message_color_3";
    public static final String WELCOME_MESSAGE_PAGINA1 = "welcome_message_page_1";
    public static final String WELCOME_MESSAGE_PAGINA2 = "welcome_message_page_2";
    public static final String WELCOME_MESSAGE_PAGINA3 = "welcome_message_page_3";
    public static final String WELCOME_IMAGEN_PAGINA1 = "welcome_image_1";
    public static final String WELCOME_IMAGEN_PAGINA2 = "welcome_image_2";
    public static final String WELCOME_IMAGEN_PAGINA3 = "welcome_image_3";




    private final SharedPreferences mSharedPreferences;
    public MConfig(Context context) {
        mSharedPreferences = context.getSharedPreferences("main_cfg", Context.MODE_PRIVATE);
    }

    public static boolean getIsDebug(){
        return ISDEBUG;
    }

    public static String getIdApp(){
        return IDAPP;
    }

    public String getApiUrl() {
        return mSharedPreferences.getString(KEY_API_URL, "http://apps.gradivusmedia.com/");
    }

    public void setApiUrl(String url) {
        mSharedPreferences.edit().putString(KEY_API_URL, url).apply();
    }

    public static String getAP() {
        return AP;
    }

    public static void setAP(String AP) {
        MConfig.AP = AP;
    }

    public static String getMOPUBID() {
        return MOPUBID;
    }

    public static long getPlacementID() {
        return placementID;
    }
}
