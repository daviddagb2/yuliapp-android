package com.gradivusmedia.yuliapp.controller;

import com.birbit.android.jobqueue.Job;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;

public interface MainController {

    void getEvents(int barId);

    void addJob(Job job);

    void getDetalleEstablecimiento(Establecimiento establecimiento);

    void getTerminos();

    void sendUserProfile(Usuario user, int TipoLogin, String UID);

}
