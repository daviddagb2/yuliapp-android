package com.gradivusmedia.yuliapp.controller;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.firebase.ui.auth.data.model.User;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.jobs.BaseJob;
import com.gradivusmedia.yuliapp.model.jobs.GetDetalleEstablecimientoJob;
import com.gradivusmedia.yuliapp.model.jobs.GetEstablecimientosJob;
import com.gradivusmedia.yuliapp.model.jobs.GetTerminosyCondicionesJob;
import com.gradivusmedia.yuliapp.model.jobs.PostUsuarioLoginJob;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;

import javax.inject.Inject;
import javax.inject.Singleton;


public class MainControllerJobImpl implements MainController{

    @Inject
    JobManager jobManager;

    Context context;

    public MainControllerJobImpl(Context context){
        DI.get().inject(this);
        this.context = context;
    }


    @Override
    public void getEvents(int barId) {
      //  addJob(GetEstablecimientosJob.newInstance(barId));
        Job getEstJob = new GetEstablecimientosJob(BaseJob.UI_HIGH, Long.parseLong("1111"));
        addJob(getEstJob);
    }

    @Override
    public void getDetalleEstablecimiento(Establecimiento establecimiento){
        Job getEstJob = new GetDetalleEstablecimientoJob(BaseJob.UI_HIGH,
                                                         Long.parseLong("1111"),
                                                         establecimiento);
        addJob(getEstJob);
    }


    @Override
    public void sendUserProfile(Usuario user, int TipoLogin, String UID){
        Job sendProfile = new PostUsuarioLoginJob(BaseJob.UI_HIGH,
                Long.parseLong("1111"),
                user,
                TipoLogin,
                UID );
        addJob(sendProfile);
    }



    @Override
    public void addJob(Job job) {
        try{
            if(jobManager!=null){
                jobManager.addJobInBackground(job);
            }
        }catch (Exception ex){
            Log.e("error", ex.toString());
        }
    }

    @Override
    public void getTerminos() {
        Job get_terminos_job = new GetTerminosyCondicionesJob(BaseJob.UI_HIGH,
                Long.parseLong("1111"));
        addJob(get_terminos_job);
    }
}
