package com.gradivusmedia.yuliapp.events;

public interface BaseEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;
}
