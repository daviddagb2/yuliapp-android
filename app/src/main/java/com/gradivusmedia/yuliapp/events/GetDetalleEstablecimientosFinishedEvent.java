package com.gradivusmedia.yuliapp.events;

import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.Imagen;
import com.gradivusmedia.yuliapp.model.pojos.Promocion;
import com.gradivusmedia.yuliapp.model.pojos.Servicio;

import java.util.List;

public class GetDetalleEstablecimientosFinishedEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    private int state;
    private List<Servicio> servicioslist;
    private List<Promocion> promocionlist;
    private List<Imagen> imageslist;
    Establecimiento establecimiento;

    public GetDetalleEstablecimientosFinishedEvent(int state,
                                                    Establecimiento establecimiento,
                                                    List<Servicio> servicioslist,
                                                    List<Promocion> promocionlist,
                                                    List<Imagen> imageslist ){
        this.state = state;
        this.servicioslist = servicioslist;
        this.promocionlist = promocionlist;
        this.imageslist = imageslist;
        this.establecimiento = establecimiento;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public List<Servicio> getServicioslist() {
        return servicioslist;
    }

    public void setServicioslist(List<Servicio> servicioslist) {
        this.servicioslist = servicioslist;
    }

    public List<Promocion> getPromocionlist() {
        return promocionlist;
    }

    public void setPromocionlist(List<Promocion> promocionlist) {
        this.promocionlist = promocionlist;
    }

    public List<Imagen> getImageslist() {
        return imageslist;
    }

    public void setImageslist(List<Imagen> imageslist) {
        this.imageslist = imageslist;
    }

    public int getState() {
        return state;
    }
}
