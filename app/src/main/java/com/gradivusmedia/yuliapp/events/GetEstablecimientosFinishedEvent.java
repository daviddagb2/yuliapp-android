package com.gradivusmedia.yuliapp.events;

import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;

import java.util.List;

public class GetEstablecimientosFinishedEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    private int state;
    private List<Establecimiento> establecimientoslist;

    public GetEstablecimientosFinishedEvent(int state, List<Establecimiento> establecimientoslist){
        state = state;
        this.establecimientoslist = establecimientoslist;
    }

    public List<Establecimiento> getEstablecimientos(){
        return establecimientoslist ;
    }

    public int getState() {
        return state;
    }
}
