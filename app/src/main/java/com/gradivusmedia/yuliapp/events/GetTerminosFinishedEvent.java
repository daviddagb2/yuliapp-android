package com.gradivusmedia.yuliapp.events;

import com.gradivusmedia.yuliapp.model.pojos.Termino;

import java.util.List;

public class GetTerminosFinishedEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    private int state;
    private List<Termino> terminoslist;

    public GetTerminosFinishedEvent(int state, List<Termino> terminoslist){
        state = state;
        this.terminoslist = terminoslist;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<Termino> getTerminoslist() {
        return terminoslist;
    }

    public void setTerminoslist(List<Termino> terminoslist) {
        this.terminoslist = terminoslist;
    }
}
