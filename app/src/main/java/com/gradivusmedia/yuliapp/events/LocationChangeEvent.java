package com.gradivusmedia.yuliapp.events;

import android.location.Location;

public class LocationChangeEvent {

    public static final int LOCATION_OK = 1;
    public static final int LOCATION_NOT_FOUND = 2;

    public Location location = null;

    public LocationChangeEvent(int state, Location location){
        state = state;
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
