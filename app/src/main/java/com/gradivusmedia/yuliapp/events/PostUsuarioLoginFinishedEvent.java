package com.gradivusmedia.yuliapp.events;

import com.gradivusmedia.yuliapp.model.pojos.UsuarioLogin;

public class PostUsuarioLoginFinishedEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    private int state;
    private UsuarioLogin userlogged;

    public PostUsuarioLoginFinishedEvent(int state, UsuarioLogin userlogged){
        state = state;
        this.userlogged = userlogged;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public UsuarioLogin getUserlogged() {
        return userlogged;
    }

    public void setUserlogged(UsuarioLogin userlogged) {
        this.userlogged = userlogged;
    }
}
