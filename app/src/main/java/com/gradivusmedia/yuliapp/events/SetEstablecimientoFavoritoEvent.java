package com.gradivusmedia.yuliapp.events;

import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito;

import java.util.List;

public class SetEstablecimientoFavoritoEvent {

    public final static int SUCCESS = 0;
    public final static int ERROR = 1;

    private int state;
    private Establecimiento establecimiento;
    private EstablecimientoFavorito favorito;

    public SetEstablecimientoFavoritoEvent(int state,Establecimiento establecimiento, boolean activo){
        state = state;
        this.establecimiento = establecimiento;

        int activoint = 1;
        if(!activo){
            activoint = 0;
        }

        favorito = new EstablecimientoFavorito(establecimiento.getId(), activoint);
        favorito.establecimiento.setTarget(establecimiento);
        //long favId = boxStore.boxFor(EstablecimientoFavorito.class).put(favorito); // puts order and custome
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public EstablecimientoFavorito getFavorito() {
        return favorito;
    }

    public void setFavorito(EstablecimientoFavorito favorito) {
        this.favorito = favorito;
    }
}
