package com.gradivusmedia.yuliapp.internal.di.Components;

import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.controller.MainControllerJobImpl;
import com.gradivusmedia.yuliapp.internal.di.Modules.AndroidModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.ApiModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.BusModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.DBModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.JobManagerModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.MainControllerModule;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.jobs.GetDetalleEstablecimientoJob;
import com.gradivusmedia.yuliapp.model.jobs.GetEstablecimientosJob;
import com.gradivusmedia.yuliapp.model.jobs.GetTerminosyCondicionesJob;
import com.gradivusmedia.yuliapp.model.jobs.PostUsuarioLoginJob;
import com.gradivusmedia.yuliapp.view.MainActivity;
import com.gradivusmedia.yuliapp.view.about.TerminosActivity;
import com.gradivusmedia.yuliapp.view.establecimientos.DetalleEstablecimientoActivity;
import com.gradivusmedia.yuliapp.view.establecimientos.EstablecimientosFavoritosFragment;
import com.gradivusmedia.yuliapp.view.establecimientos.EstablecimientosFragment;
import com.gradivusmedia.yuliapp.view.maps.CustomGpsMyLocationProvider;
import com.gradivusmedia.yuliapp.view.maps.MapsFragment;
import com.gradivusmedia.yuliapp.view.profile.UserProfileFragment;

import javax.inject.Singleton;
import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(
        modules={
                AndroidModule.class,
                ApiModule.class,
                BusModule.class,
                JobManagerModule.class,
                MainControllerModule.class,
                DBModule.class
        })
public interface ApplicationComponent extends AndroidInjector<MainApplication>{

    //Inject the Activities
    void inject(MainActivity main_act);
    void inject(DetalleEstablecimientoActivity detactivity);
    void inject(TerminosActivity terminosactivity);

    //Inject Fragments
    void inject(EstablecimientosFragment fragment);
    void inject(MapsFragment fragment);
    void inject(EstablecimientosFavoritosFragment fragment);
    void inject(UserProfileFragment fragmentUser);

    //Inject Jobs
    void inject(GetEstablecimientosJob job);
    void inject(GetDetalleEstablecimientoJob job);
    void inject(GetTerminosyCondicionesJob job);
    void inject(PostUsuarioLoginJob job);



    // Inject extras
    void inject(MainControllerJobImpl mainControllerJob);
    void inject(DBService serviceDB);
    void inject(CustomGpsMyLocationProvider provider);

}