package com.gradivusmedia.yuliapp.internal.di;

import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.internal.di.Components.ApplicationComponent;
import com.gradivusmedia.yuliapp.internal.di.Components.DaggerApplicationComponent;
import com.gradivusmedia.yuliapp.internal.di.Modules.AndroidModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.ApiModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.BusModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.DBModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.JobManagerModule;
import com.gradivusmedia.yuliapp.internal.di.Modules.MainControllerModule;

import org.greenrobot.eventbus.EventBus;


public class DI{

    private static ApplicationComponent component;

    public static ApplicationComponent buildWithProdModules(MainApplication app){

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(app))
                .apiModule(new ApiModule())
                .busModule(new BusModule())
                .jobManagerModule(new JobManagerModule(app))
                .mainControllerModule(new MainControllerModule())
                .dBModule(new DBModule(app))
                .build();

        return component;
    }

    public static ApplicationComponent get() {
        return component;
    }

    public static void setComponent(ApplicationComponent component) {
        DI.component = component;
    }







}