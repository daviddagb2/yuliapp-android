package com.gradivusmedia.yuliapp.internal.di.Modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.config.MConfig;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule {

    private final MainApplication application;

    public AndroidModule(MainApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication(){ return application; }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    Resources provideResources(Context context){
        return context.getResources();
    }


    //-------------------------------------------------
    @Provides
    @Singleton
    public MConfig provideConfig() {
        return new MConfig(application);
    }



}
