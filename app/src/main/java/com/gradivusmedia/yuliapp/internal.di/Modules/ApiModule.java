package com.gradivusmedia.yuliapp.internal.di.Modules;

import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.model.api.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {


    @Provides
    @Singleton
    ApiService provideApiService(MConfig config){

        String BASEURL = config.getApiUrl();

        return new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);

        // return new ApiService(); // EventBus.getDefault();
    }

}
