package com.gradivusmedia.yuliapp.internal.di.Modules;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BusModule {

    /*@Provides
    @Singleton
    EventBus provideEventBus(){

        return EventBus.getDefault();
    } */

    @Provides
    @Singleton
    public EventBus eventBus() {
        return new EventBus();
    }

}
