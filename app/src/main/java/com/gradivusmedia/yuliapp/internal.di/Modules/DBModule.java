package com.gradivusmedia.yuliapp.internal.di.Modules;

import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.model.pojos.MyObjectBox;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.objectbox.BoxStore;

@Module
public class DBModule {

    MainApplication application;

    public DBModule(MainApplication application){
        this.application = application;
    }

    @Provides
    @Singleton
    BoxStore provideMyObjectBox(){

       BoxStore boxStore = MyObjectBox.builder().androidContext(application.getContext()).build();
       return  boxStore;
    }

}
