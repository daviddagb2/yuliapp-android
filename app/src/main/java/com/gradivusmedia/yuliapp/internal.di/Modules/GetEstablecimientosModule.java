package com.gradivusmedia.yuliapp.internal.di.Modules;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GetEstablecimientosModule {

    @Provides
    @Singleton
    public JobManager getJobManager(Context context) {
        Configuration config = new Configuration.Builder(context)
                .consumerKeepAlive(45)
                .maxConsumerCount(3)
                .minConsumerCount(1)
                .build();
        return new JobManager(config);
    }

}
