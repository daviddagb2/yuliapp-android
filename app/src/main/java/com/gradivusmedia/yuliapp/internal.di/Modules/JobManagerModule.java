package com.gradivusmedia.yuliapp.internal.di.Modules;

import android.content.Context;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.di.DependencyInjector;
import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.jobs.BaseJob;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class JobManagerModule {

   // private Context ctx;
    MainApplication mApp;
    public JobManagerModule( MainApplication mApp) {
       this.mApp = mApp;
    }

    @Provides
    @Singleton
    public JobManager provideJobManager(){

       /* Configuration configuration = new Configuration.Builder(MainApplication.getContext())
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minute
                .build();
        return new JobManager(configuration);*/

        Configuration config = new Configuration.Builder(mApp)
                .consumerKeepAlive(45)
                .maxConsumerCount(3)
                .minConsumerCount(1)
               // .customLogger(L.getJobLogger())
                .injector(new DependencyInjector() {
                    @Override
                    public void inject(Job job) {
                        if (job instanceof BaseJob) {
                            ((BaseJob) job).inject(DI.get());
                        }
                    }
                })
                .build();
        return new JobManager(config);
    }

}
