package com.gradivusmedia.yuliapp.internal.di.Modules;


import android.content.Context;
import android.util.Log;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.controller.MainControllerJobImpl;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public class MainControllerModule {

    @Provides
    @Singleton
    MainController provideMainController(Context ctx){

        Log.v("TAG_MAINCONTROL", "" + ctx.getPackageName().toString());
        if(ctx.getPackageName().toString().equals("")){

        }
        MainControllerJobImpl maincontrol = new MainControllerJobImpl(ctx);

        return  maincontrol;
    }

}