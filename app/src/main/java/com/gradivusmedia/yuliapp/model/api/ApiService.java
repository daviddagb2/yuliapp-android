package com.gradivusmedia.yuliapp.model.api;

import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoDetalle;
import com.gradivusmedia.yuliapp.model.pojos.Termino;
import com.gradivusmedia.yuliapp.model.pojos.UsuarioLogin;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    @GET("/api/v1/establecimientos/1/")
    Call<List<Establecimiento>> getAllEstablecimientos();


    @GET("/api/v1/establecimientos/{idapp}/")
    Call<List<Establecimiento>> getAllEstablecimientos(
                                @Path("idapp") String idapp);


    @GET("/api/v1/establecimientos/{idapp}/{idest}/")
    Call<EstablecimientoDetalle> getDetalleEstablecimiento(
            @Path("idapp") String idapp,
            @Path("idest") String idest);


    @GET("/api/v1/terminos/{idapp}/")
    Call<List<Termino>> getAllTerminos(
            @Path("idapp") String idapp);


    @POST("/api/loginregister/")
    Call<UsuarioLogin> postUsuarioLogin( @Query("tipo_user_id") int tipo_user_id,
                                          @Query("nickname") String nickname,
                                          @Query("name")  String name,
                                          @Query("email") String email,
                                          @Query("password") String password,
                                          @Query("password_confirmation") String password_confirmation,
                                          @Query("picture") String picture,
                                          @Query("activo") int activo,
                                          @Query("ap") String ap,
                                          @Query("tipologin") int tipologin,
                                          @Query("uid") String uid);


    //Call<Establecimiento> feed(@Query("since") long since);
    /*@GET("/user_feed/{userId}.json")
    Call<FeedResponse> userFeed(@Path("userId") long userId, @Query("since") long since);

     */

}

