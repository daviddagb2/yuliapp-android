package com.gradivusmedia.yuliapp.model.db;

import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.internal.di.Modules.DBModule;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito_;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento_;
import com.gradivusmedia.yuliapp.model.pojos.Termino;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;
import com.gradivusmedia.yuliapp.model.pojos.Usuario_;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.QueryBuilder;

public class DBService {

    @Inject
    transient BoxStore boxStore;

    public DBService(){
        DI.get().inject(this);
    }

    public List<Establecimiento> getEstablecimientosBD(){
        List<Establecimiento> establecimientoslist;

        try{
            //Se crea el Box de consulta
            Box<Establecimiento> EstablecimientoBox = boxStore.boxFor(Establecimiento.class);

            //Se crea el querybuilder
            QueryBuilder<Establecimiento> builder = EstablecimientoBox.query();
            builder.equal(Establecimiento_.activo, 1);
            establecimientoslist = builder.build().find();

        }catch (Exception ex){
            establecimientoslist = new ArrayList<>();
        }
        return establecimientoslist ;
    }


    public List<Establecimiento> getEstablecimientosBD(String search){
        List<Establecimiento> establecimientoslist;

        try{
            //Se crea el Box de consulta
            Box<Establecimiento> EstablecimientoBox = boxStore.boxFor(Establecimiento.class);

            //Se crea el querybuilder
            QueryBuilder<Establecimiento> builder = EstablecimientoBox.query();
            builder.equal(Establecimiento_.activo, 1);
            builder.contains(Establecimiento_.nombre, search );

            establecimientoslist = builder.build().find();

        }catch (Exception ex){
            establecimientoslist = new ArrayList<>();
        }
        return establecimientoslist ;
    }



    public boolean checkEstablecimientoFavoritoBD(long id){
        List<EstablecimientoFavorito> establecimientoslist;
        boolean favorito = true;

        try{
            //Se crea el Box de consulta
            Box<EstablecimientoFavorito> EstablecimientoFavoritoBox = boxStore.boxFor(EstablecimientoFavorito.class);

            //Se crea el querybuilder
            QueryBuilder<EstablecimientoFavorito> builder = EstablecimientoFavoritoBox.query();
           // builder.equal(EstablecimientoFavorito_.activo, 1);
            builder.equal(EstablecimientoFavorito_.id, id);

            establecimientoslist = builder.build().find();

            if(establecimientoslist.size() > 0){
                favorito = true;
            }else{
                favorito = false;
            }

        }catch (Exception ex){
            favorito = false;
        }
        return favorito ;
    }


    public List<EstablecimientoFavorito> getEstablecimientosFavoritosBD(){
        List<EstablecimientoFavorito> establecimientoslist;

        try{
            //Se crea el Box de consulta
            Box<EstablecimientoFavorito> EstablecimientoFavBox = boxStore.boxFor(EstablecimientoFavorito.class);

            //Se crea el querybuilder
            QueryBuilder<EstablecimientoFavorito> builder = EstablecimientoFavBox.query();
           // builder.equal(EstablecimientoFavorito_.activo, 1);
            establecimientoslist = builder.build().find();

        }catch (Exception ex){
            establecimientoslist = new ArrayList<>();
        }
        return establecimientoslist ;
    }


    public Usuario getUserActual(){
        Usuario useractual = null;
        List<Usuario> userlist;
        try{
            //Se crea el Box de consulta
            Box<Usuario> UsuarioBox = boxStore.boxFor(Usuario.class);

            //Se crea el querybuilder
            QueryBuilder<Usuario> builder = UsuarioBox.query();
            builder.equal(Usuario_.activo, 1);
            builder.notEqual(Usuario_.id, 0);
            userlist = builder.build().find();

            useractual = userlist.get(0);

        }catch (Exception ex){
            userlist = null;
        }

        return  useractual;
    }

    public boolean deleteAllUsers(){
        try{
            //Se crea el Box de consulta
            Box<Usuario> UsuarioBox = boxStore.boxFor(Usuario.class);

            UsuarioBox.removeAll();

            return true;

        }catch (Exception ex){
            return false;
        }
    }


    public Termino getTerminosAppBD(){
        Termino detalleTerminos = null;
        List<Termino> terminoslist;
        try{
            //Se crea el Box de consulta
            Box<Termino> EstablecimientoFavBox = boxStore.boxFor(Termino.class);

            //Se crea el querybuilder
            QueryBuilder<Termino> builder = EstablecimientoFavBox.query();
            // builder.equal(EstablecimientoFavorito_.activo, 1);
            terminoslist = builder.build().find();

            detalleTerminos = terminoslist.get(0);

        }catch (Exception ex){
            detalleTerminos = null;
        }
        return  detalleTerminos;
    }



}
