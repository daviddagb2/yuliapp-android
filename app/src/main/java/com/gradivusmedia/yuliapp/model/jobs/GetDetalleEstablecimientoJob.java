package com.gradivusmedia.yuliapp.model.jobs;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.events.GetDetalleEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.Components.ApplicationComponent;
import com.gradivusmedia.yuliapp.model.api.ApiService;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoDetalle;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import io.objectbox.BoxStore;
import retrofit2.Call;

public class GetDetalleEstablecimientoJob extends BaseJob {

    private static final String GROUP = "DetalleEstablecimientoJob";
    private final Long mUserId;

    @Inject
    transient ApiService apiService;

    @Inject
    transient EventBus mEventBus;

    @Inject
    transient BoxStore boxStore;

    private int barId;
    public static final int PRIORITY = 1;

    Establecimiento establecimiento;


    public GetDetalleEstablecimientoJob(@Priority int priority, @Nullable Long userId, Establecimiento establecimiento) {
        super(new Params(priority).addTags(GROUP).requireNetwork());
        mUserId = userId;
        this.establecimiento = establecimiento;
    }

    @Override
    public void inject(ApplicationComponent appComponent) {
        super.inject(appComponent);
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {
        try{
            Call<EstablecimientoDetalle> call = apiService.getDetalleEstablecimiento(
                    MConfig.getIdApp(),
                    String.valueOf(establecimiento.getId()));
            EstablecimientoDetalle establecimientosdet = call.execute().body();

            //Send the event with the list of events
            mEventBus.post(new GetDetalleEstablecimientosFinishedEvent(GetDetalleEstablecimientosFinishedEvent.SUCCESS,
                    establecimientosdet.getEstablecimiento(),
                    establecimientosdet.getServicios(),
                    establecimientosdet.getPromociones(),
                    establecimientosdet.getImages() ));

        }catch (Exception ex){
            //Error handling
            mEventBus.post(new GetDetalleEstablecimientosFinishedEvent(
                    GetDetalleEstablecimientosFinishedEvent.ERROR,
                    null,
                    null,
                    null,
                    null));
        }

    }

    @Override
    public void onRun() throws Throwable {

    }

        @Override
    protected void onCancel(int cancelReason, @android.support.annotation.Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
      /*  if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        } */
        return RetryConstraint.CANCEL;
    }
}
