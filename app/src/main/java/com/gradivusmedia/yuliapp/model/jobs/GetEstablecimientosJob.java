package com.gradivusmedia.yuliapp.model.jobs;


import android.support.annotation.NonNull;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.Components.ApplicationComponent;
import com.gradivusmedia.yuliapp.model.api.ApiService;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import org.greenrobot.eventbus.EventBus;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Inject;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import retrofit2.Call;

public class GetEstablecimientosJob extends BaseJob {

    private static final String GROUP = "FetchFeedJob";
    private final Long mUserId;

    @Inject
    transient ApiService apiService;

    @Inject
    transient EventBus mEventBus;

    @Inject
    transient BoxStore boxStore;

    private int barId;
    public static final int PRIORITY = 1;

    public GetEstablecimientosJob(@Priority int priority, @Nullable Long userId) {
        super(new Params(priority).addTags(GROUP).requireNetwork());
        mUserId = userId;
    }

    @Override
    public void inject(ApplicationComponent appComponent) {
        super.inject(appComponent);
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {

        try{
              Call<List<Establecimiento>> call = apiService.getAllEstablecimientos(MConfig.getIdApp());
            List<Establecimiento> establecimientos = call.execute().body();

            if(establecimientos.size() > 0){
                //Recorremos el Array y guardamos el ObjectBox
                for (Establecimiento item: establecimientos) {
                    try{
                        //-----------------------------------------------------------------
                        //creas una caja y la Instancias en base al objeto Inyectado
                        Box<Establecimiento> EstablecimientoBox = boxStore.boxFor(Establecimiento.class);
                        EstablecimientoBox.put(item);
                        Log.d("GetEstablecimientosJob", "Inserted new est, ID: " + item.getId());
                        //-----------------------------------------------------------------
                    }catch (Exception ex){
                        Log.e("AddBoxEst", ex.toString());
                    }
                }
            }



            //PARA LLAMADAS ASYNCRONAS
            /*    call.enqueue(new Callback<List<Establecimiento>>() {
                @Override
                public void onResponse(Call<List<Establecimiento>> call, Response<List<Establecimiento>> response) {
                    //progressDoalog.dismiss();
                    generateDataList(response.body());
                }

                @Override
                public void onFailure(Call<List<Establecimiento>> call, Throwable t) {

                }
            });   */

           /*
            List<Establecimiento> establecimientos;
            establecimientos = new ArrayList<>();
            for (int i= 0; i<= 20; i++){
                Establecimiento est = new Establecimiento();
                est.setActivo(1);
                est.setAplicacionId(1);
                est.setCelular("888888" + i);
                est.setCreatedAt("2018-09-12 20:53:02");
                est.setUpdatedAt("2018-09-12 20:53:02");
                est.setDescripcion("Descripcion del local " + 1);
                est.setDireccion("Managua donde fue el arbolito");
                est.setEmail("email" + i + "@gmail.com");
                est.setId(i);
                est.setImagen("http://www.sportshotel.cn/images/banner_pic24.jpg");
                est.setLat("12.1150");
                est.setLon("86.2362");
                est.setNombre("Establecimiento " + i);
                est.setPuntuacion(5);
                est.setTelefono("2222222");
                est.setWebsite("http://google.com");
                est.setTipoEstablecimientoId(1);

                establecimientos.add(est);

                try{
                    if(est.getId() != 0){
                        //-----------------------------------------------------------------
                        //creas una caja y la Instancias en base al objeto Inyectado
                        // Box estmodel = boxStore.boxFor(EstablecimientoModel.class);
                        Box<Establecimiento> EstablecimientoBox = boxStore.boxFor(Establecimiento.class);
                        EstablecimientoBox.put(est);
                        Log.d("GetEstablecimientosJob", "Inserted new est, ID: " + est.getId());
                        //-----------------------------------------------------------------
                    }
                }catch (Exception ex){
                    Log.e("AddBoxEst", ex.toString());
                }

            } //Aaui el end
           // ==============================================================================
            */
            //Send the event with the list of events
            mEventBus.post(new GetEstablecimientosFinishedEvent(GetEstablecimientosFinishedEvent.SUCCESS, establecimientos));

        }catch(Exception e){
            //Error handling
            mEventBus.post(new GetEstablecimientosFinishedEvent( GetEstablecimientosFinishedEvent.ERROR,null));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @android.support.annotation.Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
      /*  if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        } */
        return RetryConstraint.CANCEL;
    }

}
