package com.gradivusmedia.yuliapp.model.jobs;

import android.support.annotation.NonNull;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.events.GetTerminosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.Components.ApplicationComponent;
import com.gradivusmedia.yuliapp.model.api.ApiService;
import com.gradivusmedia.yuliapp.model.pojos.Termino;

import org.greenrobot.eventbus.EventBus;
import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Inject;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import retrofit2.Call;

public class GetTerminosyCondicionesJob extends BaseJob {


    private static final String GROUP = "GetTerminosyJob";
    private final Long mUserId;

    @Inject
    transient ApiService apiService;

    @Inject
    transient EventBus mEventBus;

    @Inject
    transient BoxStore boxStore;

    private int barId;
    public static final int PRIORITY = 1;

    public GetTerminosyCondicionesJob(@Priority int priority, @Nullable Long userId) {
        super(new Params(priority).addTags(GROUP).requireNetwork());
        mUserId = userId;
    }

    @Override
    public void inject(ApplicationComponent appComponent) {
        super.inject(appComponent);
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        try{

            Call<List<Termino>> call = apiService.getAllTerminos(MConfig.getIdApp());
            List<Termino> terminos = call.execute().body();

            if(terminos.size() > 0){
                //Recorremos el Array y guardamos el ObjectBox
                for (Termino item: terminos) {
                    try{
                        //-----------------------------------------------------------------
                        //creas una caja y la Instancias en base al objeto Inyectado
                        Box<Termino> TerminosBox = boxStore.boxFor(Termino.class);
                        TerminosBox.put(item);
                        Log.d("" + GROUP, "Inserted new trm, ID: " + item.getId());
                        //-----------------------------------------------------------------
                    }catch (Exception ex){
                        Log.e("AddBoxTrm", ex.toString());
                    }
                }
            }

            //Send the event with the list of events
            mEventBus.post(new GetTerminosFinishedEvent( GetTerminosFinishedEvent.SUCCESS, terminos));

        }catch(Exception e){
            //Error handling
            mEventBus.post(new GetTerminosFinishedEvent( GetTerminosFinishedEvent.ERROR,null));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @android.support.annotation.Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.CANCEL;
    }


}
