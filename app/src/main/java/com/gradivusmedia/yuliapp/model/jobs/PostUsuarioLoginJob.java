package com.gradivusmedia.yuliapp.model.jobs;

import android.support.annotation.NonNull;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.events.GetTerminosFinishedEvent;
import com.gradivusmedia.yuliapp.events.PostUsuarioLoginFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.Components.ApplicationComponent;
import com.gradivusmedia.yuliapp.model.api.ApiService;
import com.gradivusmedia.yuliapp.model.pojos.Termino;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;
import com.gradivusmedia.yuliapp.model.pojos.UsuarioLogin;

import org.greenrobot.eventbus.EventBus;
import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Inject;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import retrofit2.Call;

public class PostUsuarioLoginJob extends BaseJob {

    private static final String GROUP = "PostUsuarioLoginJob";
    private final Long mUserId;

    @Inject
    transient ApiService apiService;

    @Inject
    transient EventBus mEventBus;

    @Inject
    transient BoxStore boxStore;

    private int barId;
    public static final int PRIORITY = 1;

    public Usuario user;
    public int TipoLogin;
    public String UID;

    public PostUsuarioLoginJob(@BaseJob.Priority int priority, @Nullable Long userId, Usuario user, int TipoLogin, String UID) {
        super(new Params(priority).addTags(GROUP).requireNetwork());
        mUserId = userId;
        this.user = user;
        this.TipoLogin = TipoLogin;
        this.UID = UID;
    }

    @Override
    public void inject(ApplicationComponent appComponent) {
        super.inject(appComponent);
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        try{
            Call<UsuarioLogin>  call = apiService.postUsuarioLogin(
            user.getTipoUserId(),
            user.getNickname(),
            user.getName(),
            user.getEmail(),
            "",
            "",
            user.getPicture(),
            user.getActivo(),
            MConfig.getAP(),
            TipoLogin,
            this.UID);


            UsuarioLogin UserLoginRegister = call.execute().body();

            if(UserLoginRegister != null){

                if(UserLoginRegister.getResponse() == 1){
                    try{
                        //-----------------------------------------------------------------
                        //creas una caja y la Instancias en base al objeto Inyectado
                        Box<Usuario> UsuarioBox = boxStore.boxFor(Usuario.class);
                        UsuarioBox.put( UserLoginRegister.getData() );
                        Log.d("" + GROUP, "Inserted new trm, ID: " + UserLoginRegister.getData().getId());
                        //-----------------------------------------------------------------
                    }catch (Exception ex){
                        Log.e("addUserLoginbox", ex.toString());
                    }
                }else{

                }
            }

            //Send the event with the list of events
            mEventBus.post(new PostUsuarioLoginFinishedEvent( PostUsuarioLoginFinishedEvent.SUCCESS, UserLoginRegister));

        }catch(Exception e){
            //Error handling
            mEventBus.post(new PostUsuarioLoginFinishedEvent( PostUsuarioLoginFinishedEvent.ERROR,null));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @android.support.annotation.Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.CANCEL;
    }

}
