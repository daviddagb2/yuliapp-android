package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Establecimiento implements Serializable {


    @Id(assignable = true)
    @Expose
    @SerializedName("id")
    private long id;

    @SerializedName("aplicacion_id")
    @Expose
    Integer aplicacionId;
    @SerializedName("tipo_establecimiento_id")
    @Expose
    private Integer tipoEstablecimientoId;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("puntuacion")
    @Expose
    private Integer puntuacion;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("telefono")
    @Expose
    private String telefono;
    @SerializedName("celular")
    @Expose
    private String celular;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imagen")
    @Expose
    private String imagen;

    @SerializedName("tipodestacado")
    @Expose
    private Integer tipodestacado;

    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("ciudad_id")
    @Expose
    private Integer ciudadId;

    /**
     * No args constructor for use in serialization
     *
     */
    public Establecimiento() {
       /* aplicacionId = 0;
        tipoEstablecimientoId = 0;
        nombre = "";
        descripcion = "";
        direccion = "";
        lon = "0";
        lat = "0";
        puntuacion = 0;
        website = "";
        telefono = "";
        celular = "";
        email = "";
        imagen = "";
        activo = 1;
        createdAt = "";
        updatedAt = "";  */
    }

    /**
     *
     * @param direccion
     * @param tipoEstablecimientoId
     * @param website
     * @param descripcion
     * @param telefono
     * @param activo
     * @param updatedAt
     * @param id
     * @param nombre
     * @param imagen
     * @param puntuacion
     * @param aplicacionId
     * @param lon
     * @param email
     * @param createdAt
     * @param lat
     * @param celular
     */
    public Establecimiento(long id, Integer aplicacionId, Integer tipoEstablecimientoId, String nombre, String descripcion, String direccion, String lon, String lat, Integer puntuacion, String website, String telefono, String celular, String email, String imagen, Integer tipodestacado, Integer activo, String createdAt, String updatedAt, Integer ciudadId) {
        super();
        this.id = id;
        this.aplicacionId = aplicacionId;
        this.tipoEstablecimientoId = tipoEstablecimientoId;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.lon = lon;
        this.lat = lat;
        this.puntuacion = puntuacion;
        this.website = website;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.imagen = imagen;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.tipodestacado = tipodestacado;
        this.ciudadId = ciudadId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(Integer aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    public Integer getTipoEstablecimientoId() {
        return tipoEstablecimientoId;
    }

    public void setTipoEstablecimientoId(Integer tipoEstablecimientoId) {
        this.tipoEstablecimientoId = tipoEstablecimientoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Integer getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(Integer puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getTipodestacado() {
        return tipodestacado;
    }

    public void setTipodestacado(Integer tipodestacado) {
        this.tipodestacado = tipodestacado;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCiudadId() {
        return ciudadId;
    }

    public void setCiudadId(Integer ciudadId) {
        this.ciudadId = ciudadId;
    }
}