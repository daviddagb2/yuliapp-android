package com.gradivusmedia.yuliapp.model.pojos;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EstablecimientoDetalle implements Serializable {


    @SerializedName("establecimiento")
    @Expose
    private Establecimiento establecimiento;
    @SerializedName("images")
    @Expose
    private List<Imagen> images = null;
    @SerializedName("servicios")
    @Expose
    private List<Servicio> servicios = null;
    @SerializedName("promociones")
    @Expose
    private List<Promocion> promociones = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public EstablecimientoDetalle() {
    }

    /**
     *
     * @param servicios
     * @param establecimiento
     * @param images
     * @param promociones
     */
    public EstablecimientoDetalle(Establecimiento establecimiento, List<Imagen> images, List<Servicio> servicios, List<Promocion> promociones) {
        super();
        this.establecimiento = establecimiento;
        this.images = images;
        this.servicios = servicios;
        this.promociones = promociones;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public List<Imagen> getImages() {
        return images;
    }

    public void setImages(List<Imagen> images) {
        this.images = images;
    }

    public List<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }

    public List<Promocion> getPromociones() {
        return promociones;
    }

    public void setPromociones(List<Promocion> promociones) {
        this.promociones = promociones;
    }

}
