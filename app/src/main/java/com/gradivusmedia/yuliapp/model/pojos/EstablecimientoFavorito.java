package com.gradivusmedia.yuliapp.model.pojos;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class EstablecimientoFavorito {

    @Id(assignable = true)
    private long id;

    public ToOne<Establecimiento> establecimiento;

    private int activo = 1;

    public EstablecimientoFavorito(){

    }

    public EstablecimientoFavorito(long id, int activo) {
        this.id = id;
        this.activo = activo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
