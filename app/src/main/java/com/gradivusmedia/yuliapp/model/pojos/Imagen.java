package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Imagen {

    @Id(assignable = true)
    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("orden")
    @Expose
    private Integer orden;
    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("establecimiento_id")
    @Expose
    private Integer establecimientoId;

    /**
     * No args constructor for use in serialization
     *
     */
    public Imagen() {

        id = 0;
        String url = "";
        orden = 0;
        activo = 0;
        createdAt = "";
        updatedAt = "";
        establecimientoId = 0;
    }

    /**
     *
     * @param updatedAt
     * @param orden
     * @param id
     * @param createdAt
     * @param establecimientoId
     * @param activo
     * @param url
     */
    public Imagen(long id, String url, Integer orden, Integer activo, String createdAt, String updatedAt, Integer establecimientoId) {
        super();
        this.id = id;
        this.url = url;
        this.orden = orden;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.establecimientoId = establecimientoId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getEstablecimientoId() {
        return establecimientoId;
    }

    public void setEstablecimientoId(Integer establecimientoId) {
        this.establecimientoId = establecimientoId;
    }

}