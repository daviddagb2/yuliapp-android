package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Promocion {

    @Id(assignable = true)
    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("establecimiento_id")
    @Expose
    private Integer establecimientoId;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("imagenpromo")
    @Expose
    private String imagenpromo;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("fechainicio")
    @Expose
    private String fechainicio;
    @SerializedName("fechafin")
    @Expose
    private String fechafin;
    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public Promocion() {
    }

    /**
     *
     * @param updatedAt
     * @param nombre
     * @param id
     * @param fechafin
     * @param createdAt
     * @param establecimientoId
     * @param descripcion
     * @param imagenpromo
     * @param activo
     * @param fechainicio
     */
    public Promocion(long id, Integer establecimientoId, String nombre, String imagenpromo, String descripcion, String fechainicio, String fechafin, Integer activo, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.establecimientoId = establecimientoId;
        this.nombre = nombre;
        this.imagenpromo = imagenpromo;
        this.descripcion = descripcion;
        this.fechainicio = fechainicio;
        this.fechafin = fechafin;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getEstablecimientoId() {
        return establecimientoId;
    }

    public void setEstablecimientoId(Integer establecimientoId) {
        this.establecimientoId = establecimientoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagenpromo() {
        return imagenpromo;
    }

    public void setImagenpromo(String imagenpromo) {
        this.imagenpromo = imagenpromo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(String fechainicio) {
        this.fechainicio = fechainicio;
    }

    public String getFechafin() {
        return fechafin;
    }

    public void setFechafin(String fechafin) {
        this.fechafin = fechafin;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}