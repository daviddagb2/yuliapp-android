package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Servicio {


    @Id(assignable = true)
    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("aplicacion_id")
    @Expose
    private Integer aplicacionId;
    @SerializedName("imagen")
    @Expose
    private String imagen;
    @SerializedName("resourceID")
    @Expose
    private Integer resourceID;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("orden")
    @Expose
    private Integer orden;
    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;


    /**
     * No args constructor for use in serialization
     *
     */
    public Servicio() {
        id = 0;
        aplicacionId = 0;
        imagen = "";
        descripcion = "";
        resourceID = 1;
        descripcion = "0";
        activo = 1;
        createdAt = "";
        updatedAt = "";
    }

    /**
     *
     * @param updatedAt
     * @param orden
     * @param id
     * @param imagen
     * @param resourceID
     * @param aplicacionId
     * @param createdAt
     * @param descripcion
     * @param activo
     */
    public Servicio(long id, Integer aplicacionId, String imagen, Integer resourceID, String descripcion, Integer orden, Integer activo, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.aplicacionId = aplicacionId;
        this.imagen = imagen;
        this.resourceID = resourceID;
        this.descripcion = descripcion;
        this.orden = orden;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(Integer aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getResourceID() {
        return resourceID;
    }

    public void setResourceID(Integer resourceID) {
        this.resourceID = resourceID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}