package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Termino {

    @Id(assignable = true)
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("aplicacion_id")
    @Expose
    private Integer aplicacionId;
    @SerializedName("contenido")
    @Expose
    private String contenido;
    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public Termino() {
    }

    /**
     *
     * @param updatedAt
     * @param id
     * @param aplicacionId
     * @param contenido
     * @param createdAt
     * @param activo
     */
    public Termino(long id, Integer aplicacionId, String contenido, Integer activo, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.aplicacionId = aplicacionId;
        this.contenido = contenido;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(Integer aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}