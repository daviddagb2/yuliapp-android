package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Usuario {

    @Id(assignable = true)
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("tipo_user_id")
    @Expose
    private Integer tipoUserId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("activo")
    @Expose
    private Integer activo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("api_token")
    @Expose
    private String apiToken;

    /**
     * No args constructor for use in serialization
     *
     */
    public Usuario() {
    }

    /**
     *
     * @param updatedAt
     * @param picture
     * @param id
     * @param apiToken
     * @param email
     * @param nickname
     * @param tipoUserId
     * @param createdAt
     * @param name
     * @param activo
     */
    public Usuario(long id, Integer tipoUserId, String name, String nickname, String picture, String email, Integer activo, String createdAt, String updatedAt, String apiToken) {
        super();
        this.id = id;
        this.tipoUserId = tipoUserId;
        this.name = name;
        this.nickname = nickname;
        this.picture = picture;
        this.email = email;
        this.activo = activo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.apiToken = apiToken;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getTipoUserId() {
        return tipoUserId;
    }

    public void setTipoUserId(Integer tipoUserId) {
        this.tipoUserId = tipoUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

}