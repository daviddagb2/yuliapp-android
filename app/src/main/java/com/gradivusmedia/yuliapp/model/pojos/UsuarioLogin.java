package com.gradivusmedia.yuliapp.model.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsuarioLogin {

    @SerializedName("response")
    @Expose
    private Integer response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Usuario data;

    /**
     * No args constructor for use in serialization
     *
     */
    public UsuarioLogin() {
    }

    /**
     *
     * @param message
     * @param response
     * @param data
     */
    public UsuarioLogin(Integer response, String message, Usuario data) {
        super();
        this.response = response;
        this.message = message;
        this.data = data;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Usuario getData() {
        return data;
    }

    public void setData(Usuario data) {
        this.data = data;
    }

}