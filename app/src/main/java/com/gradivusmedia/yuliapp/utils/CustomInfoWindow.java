package com.gradivusmedia.yuliapp.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.view.establecimientos.DetalleEstablecimientoActivity;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

public class CustomInfoWindow extends MarkerInfoWindow {

    Context ctx;
    Establecimiento estab;

    public CustomInfoWindow(MapView mapView, final Context ctx) {
        super(org.osmdroid.library.R.layout.bonuspack_bubble, mapView);

        this.ctx = ctx;

        Button btn = (Button)(mView.findViewById(org.osmdroid.library.R.id.bubble_moreinfo));
        btn.setVisibility(View.GONE);

        Drawable imagebtn = ctx.getResources().getDrawable(R.drawable.round_pageview_black_18dp);
        btn.setBackground(   imagebtn);
        btn.setWidth(40);
        btn.setHeight(40);

        ImageView img_foto = (ImageView) (mView.findViewById(org.osmdroid.library.R.id.bubble_image));

        img_foto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try{
                    Intent mIntent = new Intent(ctx, DetalleEstablecimientoActivity.class);
                    Bundle mBundle = new Bundle();
                    mIntent.putExtra("establecimiento", estab); // enviamos el objeto serializado
                    mIntent.putExtras(mBundle);
                    ctx.startActivity(mIntent);
                }catch (Exception ex){

                }
            }
        });
    }

    @Override public void onOpen(Object item){
        super.onOpen(item);
       /// mView.findViewById(org.osmdroid.library.R.id.bubble_moreinfo).setVisibility(View.VISIBLE);

        ImageView img_foto = (ImageView) (mView.findViewById(org.osmdroid.library.R.id.bubble_image));

        Glide.with(ctx)
                .load(estab.getImagen())
                .into(img_foto);

    }


    public Establecimiento getEstab() {
        return estab;
    }

    public void setEstab(Establecimiento estab) {
        this.estab = estab;
    }
}