package com.gradivusmedia.yuliapp.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.BaseEvent;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.internal.di.Modules.JobManagerModule;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.view.adapters.rvEstablecimientosAdapter;

import android.support.v4.app.Fragment;
import android.widget.TextView;
import android.widget.Toast;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;


public class EstablecimientosFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.txttitulo)
    TextView txttitulo;

    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    Context ctx;

    private rvEstablecimientosAdapter mAdapter;
    private LinearLayoutManager llm;
    private List<Establecimiento> establecimientoslist;

    //Static factory method
    public static EstablecimientosFragment newInstance(Bundle args){
        EstablecimientosFragment f = new EstablecimientosFragment();
        f.setArguments(args);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_establecimientos, container, false);
        //Bind view
        ButterKnife.bind(this,view);

        ctx = getContext();

        txttitulo.setText("Establecimientos mas populares");

        list.setHasFixedSize(true);
        //Inicializar el Linear Layout Manager
        llm = new LinearLayoutManager(getActivity());
        list.setLayoutManager(llm);


        establecimientoslist = new ArrayList<>();
        //Crear el Adapter
        mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
        list.setAdapter(mAdapter);

        //Init params on created
        onViewCreated();

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_establecimientos, container, false);
        return view;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }




    public void onViewCreated(){

        //Inject dependencies
        initInjector();

        //Register event bus
        bus.register(this);

        //Init events list
      ///  initEstablecimientosList();

        //Get events
        getEvents();
    }

    // Called when the view is destroyed, i normally use this method to unbind view, and
    // unregister the event bus
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            if (bus.isRegistered(this)) bus.unregister(this);
        }catch (Exception ex){

        }
    }

    // Inject the fragment, for those that never used Dagger ignore this for now
    private void initInjector(){
        DI.get().inject(this);

        //DI.get().inject(new JobManagerModule(getContext()).provideJobManager());

    }

    //Get events, the view will only talk to the controller, that will take care of the request
    private void getEvents(){
        try{
            ////// controller.getEvents(Registry.getBarID());
            controller.getEvents(1);
        }catch (Exception ex){
            Log.e("getEvents", ex.toString());
        }
    }

    private void initEstablecimientosList(){

        establecimientoslist = new ArrayList<>();
        //Crear el Adapter
        mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
        list.setAdapter(mAdapter);

    }


    //Event listener for GetEventsFinishedEvent, this will be called when the model posts this
    //event
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(GetEstablecimientosFinishedEvent e){
        try{
            Log.v("onEventMainThread","Received the events");
            if(e.getState() == GetEstablecimientosFinishedEvent.SUCCESS){
                //mAdapter.setEstablecimientos(e.getEstablecimientos());
                establecimientoslist = e.getEstablecimientos();
                mAdapter.notifyDataSetChanged();

                mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist);
                list.setAdapter(mAdapter);
            }else{
                //Warn the user, about the error
                Toast.makeText(getContext(), "No se pudo obtener los Establecimientos", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }

    }

}
