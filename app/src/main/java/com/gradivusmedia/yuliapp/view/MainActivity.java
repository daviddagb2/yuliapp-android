package com.gradivusmedia.yuliapp.view;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.utils.CustomViewPager;
import com.gradivusmedia.yuliapp.utils.Funciones;
import com.gradivusmedia.yuliapp.view.about.AboutActivity;
import com.gradivusmedia.yuliapp.view.about.TerminosActivity;
import com.gradivusmedia.yuliapp.view.establecimientos.DetalleEstablecimientoActivity;
import com.gradivusmedia.yuliapp.view.establecimientos.EstablecimientosFavoritosFragment;
import com.gradivusmedia.yuliapp.view.establecimientos.EstablecimientosFragment;
import com.gradivusmedia.yuliapp.view.maps.MapsFragment;
import com.gradivusmedia.yuliapp.view.onboarding.PresentacionActivity;
import com.gradivusmedia.yuliapp.view.profile.UserProfileFragment;
import com.inmobi.sdk.InMobiSdk;
import com.kobakei.ratethisapp.RateThisApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.sliding_tabs)
    TabLayout sliding_tabs;

    @BindView(R.id.viewpager)
    CustomViewPager viewPager;

    @BindView(R.id.rllayout)
    RelativeLayout rllayout;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    SearchView searchView;


    @BindString(R.string.str_cerrar_salir_app_confirmar) String str_cerrar_salir_app_confirmar;

    @BindString(R.string.str_yes) String strYes;

    @BindString(R.string.str_no) String strNo;

    @BindString(R.string.app_name) String strAppName;

    //-------------------------------
    /*String dialogCerrarSesion = ctx.getString(R.string.str_cerrar_sesion_confirmar);
    String strYes = ctx.getString(R.string.str_yes);
    String strNo = ctx.getString(R.string.str_no);
    String strAppName = ctx.getString(R.string.app_name); */

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    boolean isUserFirstTime;


    @Inject
    MainController controller;

    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //------------------------------------------------------------
        //Si es la primer vez que se lanza la app
        isUserFirstTime = Boolean.valueOf(Funciones.readSharedSetting(MainActivity.this, PREF_USER_FIRST_TIME, "true"));
        Intent introIntent = new Intent(MainActivity.this, PresentacionActivity.class);
        introIntent.putExtra(PREF_USER_FIRST_TIME, isUserFirstTime);
        if (isUserFirstTime)
            startActivity(introIntent);
        //------------------------------------------------------------

        //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_main_drawer);

        setTheme(R.style.Theme_AppCompat_NoActionBar);

        //-----------------------------------------------
        //Action Bar settings
        //-----------------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        ButterKnife.bind(this);

        ctx = MainActivity.this;

        //-------------------------------------------------
        // NAVIGATION DRAWER ....
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        //-------------------------------------------------

        //Inject dependencies
        initInjector();

        initAdInMobi();

        //Obtain the registers from WS
        getEstablecimientos();

        InitUI();
       // pedirPermisos();

        // Monitor launch times and interval from installation
        RateThisApp.onCreate(this);
        // If the condition is satisfied, "Rate this app" dialog will be shown
        RateThisApp.showRateDialogIfNeeded(this);

        Funciones.setColorStatusBar(this.getWindow(), this, R.color.colorPrimaryDark);
    }


    private void initAdInMobi(){
        JSONObject consentObject = new JSONObject();
        try {
            // Provide correct consent value to sdk which is obtained by User
            consentObject.put(InMobiSdk.IM_GDPR_CONSENT_AVAILABLE, true);
            // Provide 0 if GDPR is not applicable and 1 if applicable
            consentObject.put("gdpr", "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InMobiSdk.init(MainActivity.this, MConfig.getMOPUBID(), consentObject);

        InMobiSdk.setGender(InMobiSdk.Gender.MALE); // or InMobiSdk.Gender.FEMALE
       // InMobiSdk.setAge(age);
        InMobiSdk.setAgeGroup(InMobiSdk.AgeGroup.BETWEEN_25_AND_29);

    }

    private void initInjector(){
        DI.get().inject(this);
    }

    //Get Establecimientos, the view will only talk to the controller, that will take care of the request
    private void getEstablecimientos(){
        try{
            controller.getEvents(1);
        }catch (Exception ex){
            Log.e("getEvents", ex.toString());
        }
    }


    private void InitUI(){
        viewPager.setEnableSwipe(false);
        setupViewPager(viewPager);

        sliding_tabs.setupWithViewPager(viewPager);

        sliding_tabs.getTabAt(0).setIcon(R.drawable.if_home_white);
        sliding_tabs.getTabAt(1).setIcon(R.drawable.round_explore_white_18dp);
        sliding_tabs.getTabAt(2).setIcon(R.drawable.if_favorite);
        sliding_tabs.getTabAt(3).setIcon(R.drawable.if_account_white);

        sliding_tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                int position = tab.getPosition();

                if(position > 0){
                    try {
                        searchView.clearFocus();
                    }catch (Exception ex){

                    }
                    if(position == 1){
                        pedirPermisos();
                    }
                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }



    final int WRITE_REQUEST_CODE = 200;
    private void pedirPermisos(){

        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_FINE_LOCATION};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, WRITE_REQUEST_CODE);
        }else{
           //// InitUI();
        }
    }


    private void reloadFragmentMapa(){
        try {
            MapsFragment.getInstance().initMap();
        }catch (Exception ex){
            Log.e("reloadFragmentMap", ex.toString());
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_REQUEST_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Permission granted.
                    //Continue with writing files...

                    //MapsFragment.getInstance().initMap();
                    /// InitUI();
                    reloadFragmentMapa();
                }
                else{
                    //Permission denied.
                    Snackbar.make(rllayout, "Necesitamos almacenar el mapas en la memoria de sus dispositivo para reducir el consumo de datos", Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(EstablecimientosFragment.getInstance(new Bundle()), "");
        adapter.addFragment(MapsFragment.getInstance(), "");
        adapter.addFragment(EstablecimientosFavoritosFragment.newInstance(new Bundle()), "");
        adapter.addFragment(UserProfileFragment.newInstance(new Bundle()), "");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            case R.id.nav_inicio:
                break;
            case R.id.nav_tutorial_inicio:
                Intent introIntent = new Intent(MainActivity.this, PresentacionActivity.class);
                introIntent.putExtra(PREF_USER_FIRST_TIME, isUserFirstTime);
                startActivity(introIntent);
                break;
            case R.id.nav_acercade:
                try{
                    Intent mIntent = new Intent(ctx, AboutActivity.class);
                    ctx.startActivity(mIntent);
                }catch (Exception ex){
                    Log.e("clickTerminos", ex.toString());
                }
                break;
            case R.id.nav_terminos:
                try{
                    Intent mIntent = new Intent(ctx, TerminosActivity.class);
                    ctx.startActivity(mIntent);
                }catch (Exception ex){
                    Log.e("clickTerminos", ex.toString());
                }
                break;
            case R.id.nav_salir:
                showExitDialog();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        // Retrieve the SearchView and plug it into SearchManager
      //  final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        // Search text changed listener
        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        Fragment establecimientofrag =  EstablecimientosFragment.getInstance(new Bundle());
                        if (establecimientofrag != null) {
                            ((EstablecimientosFragment) establecimientofrag).initEstablecimientosList(newText.toString());
                        }
                        return false;
                    }
                }

        );


            /*
                . .addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Fragment mainFragment = getFragmentManager().findFragmentById(R.id.container);
                if (mainFragment != null && mainFragment instanceof MainListFragment) {
                    ((MainListFragment) mainFragment).search(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // http://stackoverflow.com/a/6438918/1692770
                if (s.toString().length() <= 0) {
                    toolbarSearchView.setHintTextColor(Color.parseColor("#b3ffffff"));
                }
            }
        }); */


        try{
            TabLayout.Tab tab = sliding_tabs.getTabAt(0);
            tab.select();

            EstablecimientosFragment.getInstance(new Bundle()).initEstablecimientosList( searchView.getQuery().toString() ) ;

        }catch (Exception ex){

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;

            case R.id.action_about:
                Intent ConfigIntent = new Intent(MainActivity.this, DetalleEstablecimientoActivity.class);

                startActivity(ConfigIntent);
                //finish();
                return true;

            case R.id.action_search:
                try{
                    TabLayout.Tab tab = sliding_tabs.getTabAt(0);
                    tab.select();
                }catch (Exception ex){

                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            showExitDialog();
        }
    }

    public void showExitDialog() {

        new AlertDialog.Builder(MainActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("" + strAppName)
                .setMessage("" + str_cerrar_salir_app_confirmar)
                .setPositiveButton("" + strYes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            finish();
                        }catch (Exception ex){
                            Log.e("clickCerrarSesion", ex.toString());
                        }
                    }

                })
                .setNegativeButton("" + strNo, null)
                .show();

    }
}
