package com.gradivusmedia.yuliapp.view.about;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetTerminosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.Termino;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.objectbox.BoxStore;

public class TerminosActivity extends AppCompatActivity {

    Context ctx;

    @BindView(R.id.ll_loading_terminos)
    LinearLayout ll_loading_terminos;

    @BindView(R.id.webView1)
    WebView webView1;


    @BindString(R.string.app_name) String titulo;

    //------------------------------------------------
    // DEPENDENCY INJECTOR
    //------------------------------------------------
    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    @Inject
    transient BoxStore boxStore;


    //----------------------------------------

    private Termino terminoDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminos);


        ButterKnife.bind(this);

        //Inject dependencies
        initInjector();

        ctx = TerminosActivity.this;

        //Set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(titulo); // set the top title
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        //Register event bus
        bus.register(this);

        getEstablecimientos();

        initTerminosList();

    }

    // Inject the fragment
    private void initInjector(){
        DI.get().inject(this);
    }

    //Get Establecimientos, the view will only talk to the controller, that will take care of the request
    private void getEstablecimientos(){
        try{
            controller.getTerminos();
        }catch (Exception ex){
            Log.e("getEvents", ex.toString());
        }
    }

    private void initTerminosList(){
        try{
            DBService service_database = new DBService();
            terminoDetalle =   service_database.getTerminosAppBD();

            if(terminoDetalle != null){
                webView1.setVisibility(View.VISIBLE);
                try{

                    webView1.loadData(terminoDetalle.getContenido(),
                            "text/html; charset=utf-8",
                            "utf-8");
                    //txtTC.setVisibility(View.VISIBLE);
                    //txtTC.setText( Html.fromHtml(terminosqlite) );
                    //TxtSinTerminos.setVisibility(View.GONE);
                }catch(Exception ex){
                    Log.e("error", "" + ex.toString());
                }
            }else{

                //then Load the Offline TC

            }

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(ctx, "Ocurrio un error", Toast.LENGTH_SHORT).show();

            webView1.setVisibility(View.GONE);
          //  rlempty.setVisibility(View.GONE);
          //  list.setVisibility(View.VISIBLE);
        }
    }


    // Para setear los Menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(GetTerminosFinishedEvent e){
        try{
            Log.v("onEventMainThread","Received the terminos");
            if(e.getState() == GetTerminosFinishedEvent.SUCCESS){

                ll_loading_terminos.setVisibility(View.GONE);

                initTerminosList();

               /* terminoslist   = e.getServicioslist();
                promocioneslist = e.getPromocionlist();
                imageslist = e.getImageslist();

                if(e.getEstablecimiento() != null){
                    establecimiento = e.getEstablecimiento();
                }
                setData();
                fillListadoServicios();
                fillListadoPromociones();

                ll_empty.setVisibility(View.GONE);
                ll_loading.setVisibility(View.GONE);
                ll_loading_servicios.setVisibility(View.GONE);
                ll_content.setVisibility(View.VISIBLE);

            }
            else{
                //Warn the user, about the error
                Toast.makeText(ctx, "No se pudieron obtener los detalles", Toast.LENGTH_SHORT).show();

                ///  ll_empty.setVisibility(View.VISIBLE);
                ll_loading.setVisibility(View.GONE);
                ll_loading_servicios.setVisibility(View.GONE);
                ll_content.setVisibility(View.VISIBLE);*/
            }
        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(ctx, "Ocurrio un error", Toast.LENGTH_SHORT).show();

            ll_loading_terminos.setVisibility(View.GONE);

            //ll_empty.setVisibility(View.GONE);
           /*
            ll_loading.setVisibility(View.GONE);
            ll_loading_servicios.setVisibility(View.GONE);
            ll_content.setVisibility(View.VISIBLE);  */
        }
    }

}
