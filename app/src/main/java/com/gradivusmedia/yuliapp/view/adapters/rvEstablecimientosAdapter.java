package com.gradivusmedia.yuliapp.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.model.pojos.AdEstablecimiento;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.view.establecimientos.DetalleEstablecimientoActivity;
import com.inmobi.ads.InMobiNative;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//RecyclerView.Adapter<rvEstablecimientosAdapter.EstablecimientosViewHolder>
public class rvEstablecimientosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final LayoutInflater mLayoutInflater;
    final Map<String, Establecimiento> mUniqueMapping = new HashMap<>();
    private Callback mCallback;
    // private List<String> mData;
    List<Establecimiento> mData;
    Context ctx;


    //View type for Content Feed.
    private static final int VIEW_TYPE_CONTENT_FEED = 0;

    //View type for Ad Feed - from InMobi (InMobi Native Strand)
    private static final int VIEW_TYPE_INMOBI_STRAND = 1;


    public rvEstablecimientosAdapter(Context context, List<Establecimiento> mData) {
        this.ctx = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.mData = mData;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }


    // RecyclerView.ViewHolder  /// EstablecimientosViewHolder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       // View view = mLayoutInflater.inflate(R.layout.item_establecimiento, parent, false);
        View card = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_layout,
                parent,
                false);

        //Para identificar el tipo de anuncio
        if (viewType == VIEW_TYPE_CONTENT_FEED) {

            View view = mLayoutInflater.inflate(R.layout.item_establecimiento, parent, false);
           /// LayoutInflater.from(parent.getContext()).inflate(R.layout.item_establecimiento, (ViewGroup) card, true);
            return new EstablecimientosViewHolder(view);

           // view = mLayoutInflater.inflate(R.layout.my_ad_layout, parent, false);
           // LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem, (ViewGroup) card, true);
           //
        } else {
            return new AdViewHolder(ctx, card);
        }

    //    return new EstablecimientosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //obtener el objeto item
        Establecimiento item = mData.get(position);

        if (holder instanceof EstablecimientosViewHolder) {
            //=============================================
            //Es un Establecimiento
            //=============================================
            EstablecimientosViewHolder estViewHolder = (EstablecimientosViewHolder) holder;

            //Es un establecimiento normal
            String nombre = mData.get(position).getNombre();
            String desc   = mData.get(position).getDescripcion();
            estViewHolder.txttitulo.setText(nombre);
            estViewHolder.txtsubtitulo.setText(desc);

            Glide.with(ctx)
                    .load(mData.get(position).getImagen())
                    .into(estViewHolder.location_picture);

            final Establecimiento estab =  mData.get(position);

            estViewHolder.rlmainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        Intent mIntent = new Intent(ctx, DetalleEstablecimientoActivity.class);
                        Bundle mBundle = new Bundle();
                        mIntent.putExtra("establecimiento", estab); // enviamos el objeto serializado
                        mIntent.putExtras(mBundle);
                        ctx.startActivity(mIntent);
                    }catch (Exception ex){

                    }
                }
            });

            if(mData.get(position).getPuntuacion() < 5){
                if(mData.get(position).getPuntuacion() <= 0 ){
                    estViewHolder.ratingBar.setNumStars(mData.get(position).getPuntuacion() );
                }else{
                    estViewHolder.ratingBar.setNumStars(0);
                }
            }else{
                estViewHolder.ratingBar.setNumStars(5);
            }


        }else{
            //=============================================
            //Es un AD
            //=============================================
            final AdViewHolder adViewHolder = (AdViewHolder) holder;
            final InMobiNative inMobiNative = ((AdEstablecimiento) item).mNativeStrand;

            Picasso.with(ctx)
                    .load(inMobiNative.getAdIconUrl())
                    .into(adViewHolder.icon);
            adViewHolder.title.setText(inMobiNative.getAdTitle());
            adViewHolder.description.setText(inMobiNative.getAdDescription());
            adViewHolder.action.setText(inMobiNative.getAdCtaText());

            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            adViewHolder.adContent.addView(inMobiNative.getPrimaryViewOfWidth(ctx, adViewHolder.adView,
                    adViewHolder.cardView, displayMetrics.widthPixels));

            float rating = inMobiNative.getAdRating();
            if (rating != 0) {
                adViewHolder.ratingBar.setRating(rating);
            }
            adViewHolder.ratingBar.setVisibility(rating != 0 ? View.VISIBLE : View.GONE);

            adViewHolder.action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inMobiNative.reportAdClickAndOpenLandingPage();
                }
            });
            //=============================================
        }
    }


    @Override
    public int getItemViewType(int position) {
        final Establecimiento item = mData.get(position);
        if (item instanceof AdEstablecimiento) {
            return VIEW_TYPE_INMOBI_STRAND;
        }
        return VIEW_TYPE_CONTENT_FEED;
    }


    @Override
    public int getItemCount() {
        if(mData != null){
            return mData.size();
        }else{
            return 0;
        }
    }




    private static class AdViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        View adView;

        ImageView icon;
        TextView title, description;
        Button action;
        FrameLayout adContent;
        RatingBar ratingBar;

        AdViewHolder(Context context, View adCardView) {
            super(adCardView);
            cardView = (CardView) adCardView;
            adView = LayoutInflater.from(context).inflate(R.layout.my_ad_layout, null);

            icon = (ImageView) adView.findViewById(R.id.adIcon);
            title = (TextView) adView.findViewById(R.id.adTitle);
            description = (TextView) adView.findViewById(R.id.adDescription);
            action = (Button) adView.findViewById(R.id.adAction);
            adContent = (FrameLayout) adView.findViewById(R.id.adContent);
            ratingBar = (RatingBar) adView.findViewById(R.id.adRating);
            cardView.addView(adView);
        }
    }






    public static class EstablecimientosViewHolder extends RecyclerView.ViewHolder {

        LinearLayout rlmainlayout;
        TextView txttitulo;
        TextView txtsubtitulo;
        ImageView location_picture;
        RatingBar ratingBar;

        public EstablecimientosViewHolder(View itemView) {
            super(itemView);
            txttitulo = itemView.findViewById(R.id.txttitulo);
            txtsubtitulo = itemView.findViewById(R.id.txtsubtitulo);
            location_picture = itemView.findViewById(R.id.location_picture);
            rlmainlayout = itemView.findViewById(R.id.rlmainlayout);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }



    public interface Callback {
        void onUserClick(Establecimiento establecimiento);
    }
}