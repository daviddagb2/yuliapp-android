package com.gradivusmedia.yuliapp.view.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito;
import com.gradivusmedia.yuliapp.view.establecimientos.DetalleEstablecimientoActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class rvEstablecimientosFavoritosAdapter extends RecyclerView.Adapter<rvEstablecimientosFavoritosAdapter.FavoritosViewHolder> {

        final LayoutInflater mLayoutInflater;

        final Map<String, EstablecimientoFavorito> mUniqueMapping = new HashMap<>();

        private Callback mCallback;

        // private List<String> mData;
        List<EstablecimientoFavorito> mData;

        Context ctx;

        public rvEstablecimientosFavoritosAdapter(Context context, List<EstablecimientoFavorito> mData) {
            this.ctx = context;
            mLayoutInflater = LayoutInflater.from(context);
            this.mData = mData;
        }

        public void setCallback(Callback callback) {
            mCallback = callback;
        }

        @Override
        public FavoritosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mLayoutInflater.inflate(R.layout.item_establecimiento_favorito, parent, false);
            return new FavoritosViewHolder(view);
        }

        @Override
        public void onBindViewHolder(FavoritosViewHolder holder, final int position) {


            final Establecimiento establecimiento = mData.get(position).establecimiento.getTarget();

           // String nombre = mData.get(position).getEstablecimiento().getNombre();
           // String desc   = mData.get(position).getEstablecimiento().getDescripcion();

            String nombre = establecimiento.getNombre();
            String desc   = establecimiento.getDescripcion();

            holder.txttitulo.setText(nombre);
            holder.txtsubtitulo.setText(desc);

            Glide.with(ctx)
                    .load(establecimiento.getImagen())
                    .into(holder.location_picture);

            holder.rlmainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        Intent mIntent = new Intent(ctx, DetalleEstablecimientoActivity.class);
                        Bundle mBundle = new Bundle();
                        mIntent.putExtra("establecimiento", establecimiento); // enviamos el objeto serializado
                        mIntent.putExtras(mBundle);
                        ctx.startActivity(mIntent);
                    }catch (Exception ex){

                    }
                }
            });

           /* if(mData.get(position).getPuntuacion() < 5){

                if(mData.get(position).getPuntuacion() <= 0 ){
                    holder.ratingBar.setNumStars(mData.get(position).getPuntuacion() );
                }else{
                    holder.ratingBar.setNumStars(0);
                }
            }else{
                holder.ratingBar.setNumStars(5);
            } */

        }

        @Override
        public int getItemCount() {
            if(mData != null){
                return mData.size();
            }else{
                return 0;
            }
        }


        public static class FavoritosViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout rlmainlayout;
            TextView txttitulo;
            TextView txtsubtitulo;
            ImageView location_picture;
            RatingBar ratingBar;

            public FavoritosViewHolder(View itemView) {
                super(itemView);
                txttitulo = itemView.findViewById(R.id.txttitulo);
                txtsubtitulo = itemView.findViewById(R.id.txtsubtitulo);
                location_picture = itemView.findViewById(R.id.location_picture);
                rlmainlayout = itemView.findViewById(R.id.rlmainlayout);
                ratingBar = itemView.findViewById(R.id.ratingBar);
            }
        }



        public interface Callback {
            void onUserClick(EstablecimientoFavorito establecimiento);
        }


}