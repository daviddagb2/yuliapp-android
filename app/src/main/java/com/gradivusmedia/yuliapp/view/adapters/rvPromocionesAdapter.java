package com.gradivusmedia.yuliapp.view.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.model.pojos.Promocion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class rvPromocionesAdapter extends RecyclerView.Adapter<rvPromocionesAdapter.PromocionesViewHolder> {

        final LayoutInflater mLayoutInflater;

        final Map<String, Promocion> mUniqueMapping = new HashMap<>();

        private Callback mCallback;

        // private List<String> mData;
        List<Promocion> mData;

        Context ctx;
        public rvPromocionesAdapter(Context context, List<Promocion> mData) {
            this.ctx = context;
            mLayoutInflater = LayoutInflater.from(context);
            this.mData = mData;
        }

        public void setCallback(Callback callback) {
            mCallback = callback;
        }

        @Override
        public PromocionesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mLayoutInflater.inflate(R.layout.item_oferta, parent, false);
            return new PromocionesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PromocionesViewHolder holder, final int position) {

            String nombre = mData.get(position).getNombre();
            String desc   = mData.get(position).getDescripcion();

            holder.txt_nombre.setText( mData.get(position).getNombre() );
            holder.txt_subtitulo.setText( mData.get(position).getDescripcion() );
            //holder.txt_detaller.setText( mData.get(position).getFechainicio() );
            //holder.txt_precio.setText( mData.get(position).getFechainicio() );
            holder.txt_precio.setVisibility(View.GONE);
            holder.txt_detaller.setVisibility(View.GONE);

           /* Glide.with(ctx)
                    .load(mData.get(position).getImagen())
                    .into(holder.location_picture);  */

            holder.rlmainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* try{
                        Intent mIntent = new Intent(ctx, DetalleEstablecimientoActivity.class);
                        Bundle mBundle = new Bundle();
                        mIntent.putExtra("establecimiento", mData.get(position)); // enviamos el objeto serializado
                        mIntent.putExtras(mBundle);
                        ctx.startActivity(mIntent);
                    }catch (Exception ex){

                    }  */
                }
            });
        }

        @Override
        public int getItemCount() {
            if(mData != null){
                return mData.size();
            }else{
                return 0;
            }
        }


        public static class PromocionesViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout rlmainlayout;
            TextView txt_nombre;
            TextView txt_subtitulo;
            TextView txt_precio;
            TextView txt_detaller;

           // ImageView location_picture;

            public PromocionesViewHolder(View itemView) {
                super(itemView);
                rlmainlayout = itemView.findViewById(R.id.rlmainlayout);
                txt_nombre = itemView.findViewById(R.id.txt_nombre);
                txt_subtitulo = itemView.findViewById(R.id.txt_subtitulo);
                txt_precio = itemView.findViewById(R.id.txt_precio);
                txt_detaller  = itemView.findViewById(R.id.txt_detaller);

                //location_picture = itemView.findViewById(R.id.location_picture);

            }
        }

        public interface Callback {
            void onUserClick(Promocion establecimiento);
        }
    }