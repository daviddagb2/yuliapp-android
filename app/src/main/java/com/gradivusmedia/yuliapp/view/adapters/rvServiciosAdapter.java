package com.gradivusmedia.yuliapp.view.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.model.pojos.Servicio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class rvServiciosAdapter extends RecyclerView.Adapter<rvServiciosAdapter.ServiciosViewHolder> {

    final LayoutInflater mLayoutInflater;

    final Map<String, Servicio> mUniqueMapping = new HashMap<>();

    private Callback mCallback;

    // private List<String> mData;
    List<Servicio> mData;

    Context ctx;
    public rvServiciosAdapter(Context context, List<Servicio> mData) {
        this.ctx = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.mData = mData;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public ServiciosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_servicio, parent, false);
        return new ServiciosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiciosViewHolder holder, final int position) {

        String desc = mData.get(position).getDescripcion();

        holder.txt_nombre.setText( desc );

        holder.image_icon.setImageResource(getResourceIcon(mData.get(position).getResourceID() ));

           /* Glide.with(ctx)
                    .load(mData.get(position).getImagen())
                    .into(holder.location_picture);  */

        holder.rlmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* try{
                        Intent mIntent = new Intent(ctx, DetalleEstablecimientoActivity.class);
                        Bundle mBundle = new Bundle();
                        mIntent.putExtra("establecimiento", mData.get(position)); // enviamos el objeto serializado
                        mIntent.putExtras(mBundle);
                        ctx.startActivity(mIntent);
                    }catch (Exception ex){

                    }  */
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mData != null){
            return mData.size();
        }else{
            return 0;
        }
    }


    public static class ServiciosViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlmainlayout;
        TextView txt_nombre;
        ImageView image_icon;

        // ImageView location_picture;

        public ServiciosViewHolder(View itemView) {
            super(itemView);
            rlmainlayout = itemView.findViewById(R.id.rlmainlayout);
            txt_nombre = itemView.findViewById(R.id.txt_nombre);
            image_icon = itemView.findViewById(R.id.image_icon);
        }
    }

    public interface Callback {
        void onUserClick(Servicio servicio);
    }

    public int getResourceIcon(int ResourceID){
        String resourceName = "round_pool_black_18dp";
        switch (ResourceID){
            case 1: resourceName = "round_pool_black_18dp";
                break;
            case 2: resourceName = "round_spa_black_18dp";
                break;
            case 3: resourceName = "round_room_service_black_18dp";
                break;
            case 4: resourceName = "round_fitness_center_black_18dp";
                break;
            case 5: resourceName = "round_free_breakfast_black_18dp";
                break;
            case 6: resourceName = "round_kitchen_black_18dp";
                break;
            case 7: resourceName = "round_hot_tub_black_18dp";
                break;
            case 8: resourceName = "round_ac_unit_black_18dp";
                break;
            case 9: resourceName = "round_airport_shuttle_black_18dp";
                break;


                default:
                    resourceName = "round_folder_black_18dp";
        }

        int id = ctx.getResources().getIdentifier(resourceName, "drawable", ctx.getPackageName());
        return id;
    }

}