package com.gradivusmedia.yuliapp.view.establecimientos;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetDetalleEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.events.SetEstablecimientoFavoritoEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito;
import com.gradivusmedia.yuliapp.model.pojos.Imagen;
import com.gradivusmedia.yuliapp.model.pojos.Promocion;
import com.gradivusmedia.yuliapp.model.pojos.Servicio;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;
import com.gradivusmedia.yuliapp.utils.Funciones;
import com.gradivusmedia.yuliapp.view.adapters.rvPromocionesAdapter;
import com.gradivusmedia.yuliapp.view.adapters.rvServiciosAdapter;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.objectbox.Box;
import io.objectbox.BoxStore;



public class DetalleEstablecimientoActivity extends AppCompatActivity {

    String strTitulo = "Detalle";

    //Datasets de detalle
    Establecimiento establecimiento;

    private List<Servicio> servicioslist;
    private List<Promocion> promocioneslist;
    private List<Imagen> imageslist;

    Context ctx;
    Bundle bundle;

    private LinearLayoutManager llm;
    private GridLayoutManager gmm;

    private rvPromocionesAdapter promoAdapter;
    private rvServiciosAdapter serviAdapter;

    boolean isFavorito = false;

    @BindView(R.id.collapsing_toolbar_image_view)
    ImageView collapsing_toolbar_image_view;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbar_layout;

    //-------------------------------------------------
    //BindViews del UI
    //-------------------------------------------------
    @BindView(R.id.lbl_descripcion)
    TextView lbl_descripcion;

    @BindView(R.id.txt_descripcion)
    TextView txt_descripcion;

    @BindView(R.id.txt_ubicacion)
    TextView txt_ubicacion;


    @BindView(R.id.lbl_telefono)
    TextView lbl_telefono;

    @BindView(R.id.txt_telefono)
    TextView txt_telefono;

    @BindView(R.id.txt_celular)
    TextView txt_celular;

    @BindView(R.id.lbl_celular)
    TextView lbl_celular;

    @BindView(R.id.lbl_email)
    TextView lbl_email;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.lbl_website)
    TextView lbl_website;

    @BindView(R.id.txt_website)
    TextView txt_website;

    @BindView(R.id.lbl_ofertas)
    TextView lbl_ofertas;

    @BindView(R.id.lbl_servicios)
    TextView lbl_servicios;

    @BindView(R.id.lbl_ubicacion)
    TextView lbl_ubicacion;

    //BUTTONS
    @BindView(R.id.btn_vermas_descripcion)
    Button btn_vermas_descripcion;

    @BindView(R.id.btn_vermas_ofertas)
    Button btn_vermas_ofertas;

    @BindView(R.id.btn_vermas_servicios)
    Button btn_vermas_servicios;

    @BindView(R.id.list_ofertas)
    RecyclerView list_ofertas;


    @BindView(R.id.list_servicios)
    RecyclerView list_servicios;

    @BindView(R.id.ll_content)
    LinearLayout ll_content;

    @BindView(R.id.ll_loading)
    LinearLayout ll_loading;

    @BindView(R.id.ll_loading_servicios)
    LinearLayout ll_loading_servicios;

    @BindView(R.id.ll_empty)
    LinearLayout ll_empty;

    //------------------------------------------------
    // DEPENDENCY INJECTOR
    //------------------------------------------------
    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    @Inject
    transient BoxStore boxStore;

    //------------------------------------------------
    //for test purposes only
    private void fillList(){
        promocioneslist = new ArrayList<>();

        for(int i=0; i<= 5; i++){
            Promocion promo = new Promocion(i,
                    2,
                    "Promocion " + i,
                    "https://images.music-worx.com/users/Promo.png",
                    "descripcion asdf aaa " + i,
                    "2018-09-13 07:00:00",
                    "2018-10-10 00:00:00",
                    1,
                    "2018-09-13 18:20:41",
                    "2018-09-13 18:20:41");
            promocioneslist.add(promo);
        }

        servicioslist = new ArrayList<>();
        for(int i=0; i<= 7; i++){
            Servicio service = new Servicio(i,
                    1,
                    "https://static.thenounproject.com/png/842366-200.png",
                    1,
                    "Descripccion " + i,
                     i,
                    1,
                    "2018-09-13 18:20:41",
                    "2018-09-13 18:20:41");
            servicioslist.add(service);
        }

        imageslist = new ArrayList<>();
        for(int i=0; i<= 6; i++){
            Imagen img = new Imagen(i,
                   "https://media.equityapartments.com/images/c_crop,x_0,y_0,w_1920,h_1080/c_fill,w_1920,h_1080/q_80/565-42/emerson-place-apartments-exterior.jpg",
                    1,
                    1,
                    "2018-09-13 18:20:41",
                    "2018-09-13 18:20:41",
                    2);

            imageslist.add(img);
        }


    }

    //-------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_establecimiento);

        //Set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(strTitulo); // set the top title
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        ///fillList();

        ButterKnife.bind(this);

        //Inject dependencies
        initInjector();

        ctx = DetalleEstablecimientoActivity.this;

        bundle = this.getIntent().getExtras();

        //Ocultar el content e inicializar el loading
       // ll_content.setVisibility(View.GONE);
        ll_empty.setVisibility(View.GONE);
        ll_loading_servicios.setVisibility(View.VISIBLE);
        ll_loading.setVisibility(View.VISIBLE);

        //Inicializar reciclerviews
        list_ofertas.setHasFixedSize(true);
        list_servicios.setHasFixedSize(true);

        //Inicializar el Linear Layout Manager
        llm = new LinearLayoutManager(ctx);
        list_ofertas.setLayoutManager(llm);

        //Inicializar el GridLayoutManager
        gmm = new GridLayoutManager(ctx, 2);
        list_servicios.setLayoutManager(gmm);

        //Ocultarlo Mientras se consultan los datos
        list_ofertas.setVisibility(View.GONE);
        list_servicios.setVisibility(View.GONE);

        //Crear el de Adapter de Promociones
        promoAdapter = new rvPromocionesAdapter(ctx, promocioneslist );
        list_ofertas.setAdapter(promoAdapter);

        //Crear el Adapter de Servicio
        serviAdapter = new rvServiciosAdapter(ctx, servicioslist );
        list_servicios.setAdapter(serviAdapter);

        try{
            //Obtenemos el objeto de establecimiento
            establecimiento = (Establecimiento) getIntent().getSerializableExtra("establecimiento");
            setData();
        }catch (Exception ex){
            Log.e("onCreate DE", ex.toString());
        }

        fillListadoPromociones();
        fillListadoServicios();

        //Register event bus
        bus.register(this);

        //Obtener el detalle del webApi
        getDetalleEstablecimientoWS();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Funciones.setColorStatusBar(this.getWindow(), this, R.color.colorPrimaryDark);

        showVerMasButtons();
    }


    private void showVerMasButtons(){
        try{
            btn_vermas_descripcion.setVisibility(View.GONE);
            btn_vermas_ofertas.setVisibility(View.GONE);
            btn_vermas_servicios.setVisibility(View.GONE);

            DBService service_database = new DBService();
            Usuario userloggedin =  service_database.getUserActual();

            //btn_vermas_descripcion.setText( userloggedin.getApiToken() + " " );

        }catch (Exception ex){

        }
    }

    //Get events, the view will only talk to the controller, that will take care of the request
    private void getDetalleEstablecimientoWS(){
        try{
            controller.getDetalleEstablecimiento(establecimiento);
        }catch (Exception ex){
            Log.e("getDetalleEstWS", ex.toString());
        }
    }

    // Inject the fragment
    private void initInjector(){
        DI.get().inject(this);
    }

    private void fillListadoPromociones(){
        try{
            //promocioneslist = e.getEstablecimientos();
            promoAdapter.notifyDataSetChanged();
            promoAdapter = new rvPromocionesAdapter(ctx, promocioneslist);
            list_ofertas.setAdapter(promoAdapter);

                if(promocioneslist != null){
                    if(promocioneslist.size() > 0){
                       // rlempty.setVisibility(View.GONE);
                        list_ofertas.setVisibility(View.VISIBLE);
                        lbl_ofertas.setVisibility(View.VISIBLE);
                        btn_vermas_ofertas.setVisibility(View.VISIBLE);
                    }else {
                        list_ofertas.setVisibility(View.GONE);
                        lbl_ofertas.setVisibility(View.GONE);
                        btn_vermas_ofertas.setVisibility(View.GONE);
                    }
                }else{
                  //  rlempty.setVisibility(View.VISIBLE);
                    list_ofertas.setVisibility(View.GONE);
                    lbl_ofertas.setVisibility(View.GONE);
                    btn_vermas_ofertas.setVisibility(View.GONE);
                }
        }catch (Exception ex){
            Log.e("fillListadoPromociones", ex.toString());
            Toast.makeText(ctx, "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }
    }

    private void fillListadoServicios(){
        try{
            //servicioslist = e.getEstablecimientos();
            serviAdapter.notifyDataSetChanged();
            serviAdapter = new rvServiciosAdapter(ctx, servicioslist);
            list_servicios.setAdapter(serviAdapter);

            if(servicioslist != null){
                if(servicioslist.size() > 0){
                    // rlempty.setVisibility(View.GONE);
                    list_servicios.setVisibility(View.VISIBLE);
                    lbl_servicios.setVisibility(View.VISIBLE);
                    btn_vermas_servicios.setVisibility(View.VISIBLE);
                }else {
                    // rlempty.setVisibility(View.VISIBLE);
                    list_servicios.setVisibility(View.GONE);
                    lbl_servicios.setVisibility(View.GONE);
                    btn_vermas_servicios.setVisibility(View.GONE);
                }
            }else{
                //  rlempty.setVisibility(View.VISIBLE);
                list_servicios.setVisibility(View.GONE);
                lbl_servicios.setVisibility(View.GONE);
                btn_vermas_servicios.setVisibility(View.GONE);
            }
        }catch (Exception ex){
            Log.e("fillListadoServicios", ex.toString());
            Toast.makeText(ctx, "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        try{
            toolbar_layout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
            toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
            strTitulo = establecimiento.getNombre();

            getSupportActionBar().setTitle(strTitulo); // set the top title

            this.txt_celular.setText(establecimiento.getCelular());
            this.txt_descripcion.setText(establecimiento.getDescripcion());
            this.txt_ubicacion.setText( establecimiento.getDireccion() );
            this.txt_email.setText(establecimiento.getEmail());
            this.txt_telefono.setText(establecimiento.getTelefono());
            this.txt_website.setText(establecimiento.getWebsite());

            Glide.with(ctx)
                    .load(establecimiento.getImagen())
                    .into(collapsing_toolbar_image_view);
        }catch (Exception ex){
            Log.e("SetData DE", ex.toString());
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(GetDetalleEstablecimientosFinishedEvent e){
        try{
            Log.v("onEventMainThread","Received the events");
            if(e.getState() == GetDetalleEstablecimientosFinishedEvent.SUCCESS){

                servicioslist   = e.getServicioslist();
                promocioneslist = e.getPromocionlist();
                imageslist = e.getImageslist();

                if(e.getEstablecimiento() != null){
                    establecimiento = e.getEstablecimiento();
                }
                setData();
                fillListadoServicios();
                fillListadoPromociones();

                ll_empty.setVisibility(View.GONE);
                ll_loading.setVisibility(View.GONE);
                ll_loading_servicios.setVisibility(View.GONE);
                ll_content.setVisibility(View.VISIBLE);

            }else{
                //Warn the user, about the error
                Toast.makeText(ctx, "No se pudieron obtener los detalles", Toast.LENGTH_SHORT).show();

              ///  ll_empty.setVisibility(View.VISIBLE);
                ll_loading.setVisibility(View.GONE);
                ll_loading_servicios.setVisibility(View.GONE);
                ll_content.setVisibility(View.VISIBLE);
            }
        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(ctx, "Ocurrio un error", Toast.LENGTH_SHORT).show();

            //ll_empty.setVisibility(View.GONE);
            ll_loading.setVisibility(View.GONE);
            ll_loading_servicios.setVisibility(View.GONE);
            ll_content.setVisibility(View.VISIBLE);
        }
    }



    MenuItem menuItem;
    // Para setear los Menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
         getMenuInflater().inflate(R.menu.menu_favoritos, menu);

         menuItem = menu.findItem(R.id.action_favorite);

        getIsFavorito();

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_refresh:
                Toast.makeText(ctx, "action_refresh", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_favorite:
                setFavorito();
                return true;

            case R.id.action_share:
                Toast.makeText(ctx, "action_share", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public static void tintMenuIcon(Context context, MenuItem item, @ColorRes int color) {
        Drawable normalDrawable = item.getIcon();
        Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
        DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(color));
        item.setIcon(wrapDrawable);
    }


    private void getIsFavorito(){
        try{
            DBService service_database = new DBService();
            isFavorito =  service_database.checkEstablecimientoFavoritoBD(establecimiento.getId());

            if(isFavorito){
                tintMenuIcon(ctx, menuItem, R.color.color3);
            }else {
                tintMenuIcon(ctx, menuItem, R.color.color_white);
            }

        }catch (Exception ex){
            Log.e("getIsFavorito", ex.toString());
        }
    }

    private void setFavorito(){
        try{
            DBService service_database = new DBService();
            isFavorito =  service_database.checkEstablecimientoFavoritoBD(establecimiento.getId());

            //-----------

            if(isFavorito){
                Box<EstablecimientoFavorito> EstablecimientoFavoritoBox = boxStore.boxFor(EstablecimientoFavorito.class);
                tintMenuIcon(ctx, menuItem, R.color.color_white);
                EstablecimientoFavorito favorito = new EstablecimientoFavorito(establecimiento.getId(), 0);
                favorito.establecimiento.setTarget(establecimiento);
                EstablecimientoFavoritoBox.remove(favorito);           // Delete

            }else {
                Box<EstablecimientoFavorito> EstablecimientoFavoritoBox = boxStore.boxFor(EstablecimientoFavorito.class);
                tintMenuIcon(ctx, menuItem, R.color.color3);
                EstablecimientoFavorito favorito = new EstablecimientoFavorito(establecimiento.getId(), 1);
                favorito.establecimiento.setTarget(establecimiento);
                EstablecimientoFavoritoBox.put(favorito);

            }

            bus.post(new SetEstablecimientoFavoritoEvent(SetEstablecimientoFavoritoEvent.SUCCESS,
                    establecimiento,
                    true));


            Toast.makeText(ctx, "Favorito establecido", Toast.LENGTH_SHORT).show();
        }catch (Exception ex){
            Log.e("setFavorito", ex.toString());
        }


    }

}
