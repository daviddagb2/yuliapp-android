package com.gradivusmedia.yuliapp.view.establecimientos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.events.SetEstablecimientoFavoritoEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.EstablecimientoFavorito;
import com.gradivusmedia.yuliapp.view.adapters.rvEstablecimientosFavoritosAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EstablecimientosFavoritosFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.ll_loading_establecimientos)
    LinearLayout ll_loading_establecimientos;

    @BindView(R.id.txttitulo)
    TextView txttitulo;

    @BindView(R.id.rlempty)
    RelativeLayout rlempty;


    @BindString(R.string.str_titulo_favorito) String title;

    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    Context ctx;

    private rvEstablecimientosFavoritosAdapter mAdapter;
    private LinearLayoutManager llm;
    private List<EstablecimientoFavorito> establecimientoslist;

    //Static factory method
    public static EstablecimientosFavoritosFragment newInstance(Bundle args){
        EstablecimientosFavoritosFragment f = new EstablecimientosFavoritosFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_establecimientos_favoritos, container, false);

        //Bind view
        ButterKnife.bind(this,view);

        ctx = getContext();

        txttitulo.setText(title);

        ll_loading_establecimientos.setVisibility(View.VISIBLE);

        list.setHasFixedSize(true);
        //Inicializar el Linear Layout Manager
        llm = new LinearLayoutManager(getActivity());
        list.setLayoutManager(llm);

        rlempty.setVisibility(View.GONE);
        list.setVisibility(View.GONE);

        establecimientoslist = new ArrayList<>();
        //Crear el Adapter
        mAdapter = new rvEstablecimientosFavoritosAdapter(ctx, establecimientoslist );
        list.setAdapter(mAdapter);

        //Init params on created
        onViewCreated();

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_establecimientos, container, false);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    public void onViewCreated(){

        //Inject dependencies
        initInjector();

        //Register event bus
        bus.register(this);

        //Init events list
        initEstablecimientosList();

        //Get events
     ///   getEvents();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            if (bus.isRegistered(this)) bus.unregister(this);
        }catch (Exception ex){

        }
    }

    // Inject the fragment, for those that never used Dagger ignore this for now
    private void initInjector(){
        DI.get().inject(this);

        //DI.get().inject(new JobManagerModule(getContext()).provideJobManager());
    }



    private void initEstablecimientosList(){

        try{
            DBService service_database = new DBService();
            establecimientoslist =   service_database.getEstablecimientosFavoritosBD();
            //establecimientoslist = new ArrayList<>();
            //Crear el Adapter
            mAdapter = new rvEstablecimientosFavoritosAdapter(ctx, establecimientoslist );
            list.setAdapter(mAdapter);

            if(establecimientoslist != null){
                if(establecimientoslist.size() > 0){
                    rlempty.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    ll_loading_establecimientos.setVisibility( View.GONE);
                }else {
                    ll_loading_establecimientos.setVisibility( View.GONE);
                    rlempty.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                }
            }else{
                ll_loading_establecimientos.setVisibility( View.GONE);
                rlempty.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
            }

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();

            rlempty.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(SetEstablecimientoFavoritoEvent e){
        try{
            Log.v("onEventMainThread","Received the events");

        }catch (Exception ex){

        }
    }

}
