package com.gradivusmedia.yuliapp.view.establecimientos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.AdEstablecimiento;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.view.adapters.rvEstablecimientosAdapter;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiNative;
import com.inmobi.ads.listeners.NativeAdEventListener;
import com.inmobi.sdk.InMobiSdk;


import android.support.v4.app.Fragment;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class EstablecimientosFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.ll_loading_establecimientos)
    LinearLayout ll_loading_establecimientos;

    @BindView(R.id.txttitulo)
    TextView txttitulo;

    @BindView(R.id.rlempty)
    RelativeLayout rlempty;

    @BindString(R.string.str_titulo_establecimientos) String title;

    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    Context ctx;

    //……………………
    //All the InMobiNativeStrand instances created for this list feed will be held here
    private List<InMobiNative> mStrands = new ArrayList<>();
   // private Map<Integer, Establecimiento> mFeedMap = new HashMap<>();

    private static final int NUM_FEED_ITEMS = 20;
    //Position in feed where the Ads needs to be placed once loaded.
    private int[] mAdPositions = new int[]{3, 8, 18};

    private static final String TAG =  EstablecimientosFragment.class.getSimpleName();
    //……………………
   /* private void initAdsNative(){
        InMobiNative nativeAd = new InMobiNative(getActivity(), MConfig.getPlacementID(), nativeAdEventListener);
        nativeAd.load();
        mStrands.add(nativeAd);
    }
 */

    private rvEstablecimientosAdapter mAdapter;
    private LinearLayoutManager llm;
    private List<Establecimiento> establecimientoslist;

    public static EstablecimientosFragment instancia;

    //Static factory method
    public static EstablecimientosFragment getInstance(Bundle args){

        if(instancia == null){
            //EstablecimientosFragment f = new EstablecimientosFragment();
            //f.setArguments(args);
            instancia = new EstablecimientosFragment();
            instancia.setArguments(args);
        }
        return instancia;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_establecimientos, container, false);
        //Bind view
        ButterKnife.bind(this,view);

        ctx = getContext();

        txttitulo.setText(title);

        ll_loading_establecimientos.setVisibility(View.VISIBLE);

        list.setHasFixedSize(true);
        //Inicializar el Linear Layout Manager
        llm = new LinearLayoutManager(getActivity());
        list.setLayoutManager(llm);

        rlempty.setVisibility(View.GONE);
        list.setVisibility(View.GONE);

        establecimientoslist = new ArrayList<>();
        //Crear el Adapter
        mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
        list.setAdapter(mAdapter);
        ///setupMopubAdsRecicler(mAdapter);

        //Init params on created
        onViewCreated();

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_establecimientos, container, false);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //mFeedItems = FeedData.generateFeedItems(NUM_FEED_ITEMS);
        //mFeedAdapter = new FeedsAdapter(mFeedItems, getActivity());
        //mRecyclerView.setAdapter(mFeedAdapter);
        //mFeedAdapter.notifyDataSetChanged();

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }




    public void onViewCreated(){

        //Inject dependencies
        initInjector();

        //Register event bus
        bus.register(this);

        //Init events list
        initEstablecimientosList();

        //Get events
        //// getEvents();
    }

    // Called when the view is destroyed, i normally use this method to unbind view, and
    // unregister the event bus
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            //Destruir el mopubadapter
            ////myMoPubAdapter.destroy();

            if (bus.isRegistered(this)) bus.unregister(this);
        }catch (Exception ex){

        }
    }

    // Inject the fragment, for those that never used Dagger ignore this for now
    private void initInjector(){
        DI.get().inject(this);
        //DI.get().inject(new JobManagerModule(getContext()).provideJobManager());
    }

    //Get events, the view will only talk to the controller, that will take care of the request
    private void getEvents(){
        try{
            controller.getEvents(1);
        }catch (Exception ex){
            Log.e("getEvents", ex.toString());
        }
    }


    private void createStrands() {
        for (int position : mAdPositions) {
            final InMobiNative nativeStrand = new InMobiNative(getActivity(),
                                                               MConfig.getPlacementID(),
                                                               new StrandAdListener(position));
            mStrands.add(nativeStrand);
        }
    }

    private void loadAds() {
        for (InMobiNative strand : mStrands) {
            strand.load();
        }
    }

    private void clearAds() {
        try{
            Iterator<Establecimiento> feedItemIterator = establecimientoslist.iterator();
            while (feedItemIterator.hasNext()) {
                final Establecimiento feedItem = feedItemIterator.next();
                if (feedItem instanceof AdEstablecimiento) {
                    feedItemIterator.remove();
                }
            }

            mAdapter.notifyDataSetChanged();
            for (InMobiNative strand : mStrands) {
                strand.destroy();
            }
            mStrands.clear();
            //mFeedMap.clear();
        }catch (Exception ex){
            Log.e("clearAds", ex.toString());
        }
    }


    private void refreshAds() {
        clearAds();
        createStrands();
        loadAds();
    }


    private void initEstablecimientosList(){
        try{
            DBService service_database = new DBService();
            establecimientoslist =   service_database.getEstablecimientosBD();
            //establecimientoslist = new ArrayList<>();
            //Crear el Adapter
            mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
            list.setAdapter(mAdapter);
            ////setupMopubAdsRecicler(mAdapter);

            ///createStrands();
            ///loadAds();
            refreshAds();

            if(establecimientoslist != null){
                if(establecimientoslist.size() > 0){
                    rlempty.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                }else {
                    rlempty.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                }
            }else{
                rlempty.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
            }

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();

            rlempty.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }


    public void initEstablecimientosList(String search){
        try{
            DBService service_database = new DBService();
            establecimientoslist =   service_database.getEstablecimientosBD(search);

            //Crear el Adapter
            mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
            list.setAdapter(mAdapter);
            //setupMopubAdsRecicler(mAdapter);

            if(establecimientoslist != null){
                if(establecimientoslist.size() > 0){
                    rlempty.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                }else {
                    rlempty.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                }
            }else{
                rlempty.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
            }

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();

            rlempty.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(GetEstablecimientosFinishedEvent e){
        try{
            Log.v("onEventMainThread","Received the events");
            if(e.getState() == GetEstablecimientosFinishedEvent.SUCCESS){

                initEstablecimientosList();

              /*  //mAdapter.setEstablecimientos(e.getEstablecimientos());
                establecimientoslist = e.getEstablecimientos();
                mAdapter.notifyDataSetChanged();

                mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist);
                list.setAdapter(mAdapter);

                if(establecimientoslist != null){
                    if(establecimientoslist.size() > 0){
                        rlempty.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                    }else {
                        rlempty.setVisibility(View.VISIBLE);
                        list.setVisibility(View.GONE);
                    }
                }else{
                    rlempty.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                }*/

            }else{
                //Warn the user, about the error
                Toast.makeText(getContext(), "No se pudo obtener los Establecimientos", Toast.LENGTH_SHORT).show();
            }

            ll_loading_establecimientos.setVisibility(View.GONE);
        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();

            ll_loading_establecimientos.setVisibility(View.GONE);
            rlempty.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }












    private final class StrandAdListener extends NativeAdEventListener {

        private int mPosition;

        public StrandAdListener(int position) {
            mPosition = position;
        }

        @Override
        public void onAdLoadSucceeded(@NonNull InMobiNative inMobiNativeStrand) {
            super.onAdLoadSucceeded(inMobiNativeStrand);
            Log.d(TAG, "Strand loaded at position " + mPosition);
            if (!establecimientoslist.isEmpty()) {
                Establecimiento oldEstablecimiento = establecimientoslist.get(mPosition);
                if(oldEstablecimiento != null){
                  //  establecimientoslist.remove(oldEstablecimiento);
                }
                AdEstablecimiento adEstablecimientoItem = new AdEstablecimiento(inMobiNativeStrand);
                establecimientoslist.add(mPosition, adEstablecimientoItem);
                mAdapter.notifyDataSetChanged();

               /* FeedData.FeedItem oldFeedItem = mFeedMap.get(mPosition);
                if (oldFeedItem != null) {
                    mFeedMap.remove(mPosition);
                    mFeedItems.remove(oldFeedItem);
                } */

                //AdFeedItem adFeedItem = new AdFeedItem(inMobiNativeStrand);
                //mFeedMap.put(mPosition, adFeedItem);
                //mFeedItems.add(mPosition, adFeedItem);
                //mFeedAdapter.notifyItemChanged(mPosition);
            }
        }

        @Override
        public void onAdLoadFailed(@NonNull InMobiNative inMobiNativeStrand, @NonNull final InMobiAdRequestStatus inMobiAdRequestStatus) {
            super.onAdLoadFailed(inMobiNativeStrand, inMobiAdRequestStatus);
            Log.d(TAG, "Ad Load failed  for" + mPosition + "(" + inMobiAdRequestStatus.getMessage() + ")");
            if (!establecimientoslist.isEmpty()) {

                Establecimiento oldEstablecimiento = establecimientoslist.get(mPosition);
                //FeedData.FeedItem oldFeedItem = mFeedMap.get(mPosition);
                if(oldEstablecimiento != null){
                    //establecimientoslist.remove(mPosition);
                   ///// establecimientoslist.remove(oldEstablecimiento);
                   //// mAdapter.notifyItemRemoved(mPosition);
                    Log.d(TAG, "Ad removed for" + mPosition);
                }

               /* if (oldFeedItem != null) {
                    mFeedMap.remove(mPosition);
                    mFeedItems.remove(oldFeedItem);
                    mFeedAdapter.notifyItemRemoved(mPosition);
                    Log.d(TAG, "Ad removed for" + mPosition);
                } */
            }
        }

        @Override
        public void onAdFullScreenDismissed(InMobiNative inMobiNative) {
            super.onAdFullScreenDismissed(inMobiNative);
        }

        @Override
        public void onAdFullScreenWillDisplay(InMobiNative inMobiNative) {
            super.onAdFullScreenWillDisplay(inMobiNative);
        }

        @Override
        public void onAdFullScreenDisplayed(InMobiNative inMobiNative) {
            super.onAdFullScreenDisplayed(inMobiNative);
        }

        @Override
        public void onUserWillLeaveApplication(InMobiNative inMobiNative) {
            super.onUserWillLeaveApplication(inMobiNative);
        }

        @Override
        public void onAdImpressed(@NonNull InMobiNative inMobiNativeStrand) {
            super.onAdImpressed(inMobiNativeStrand);
            Log.d(TAG, "Impression recorded for strand at position:" + mPosition);
        }

        @Override
        public void onAdClicked(@NonNull InMobiNative inMobiNativeStrand) {
            super.onAdClicked(inMobiNativeStrand);
            Log.d(TAG, "Click recorded for ad at position:" + mPosition);
        }

        @Override
        public void onAdStatusChanged(@NonNull InMobiNative inMobiNative) {
            super.onAdStatusChanged(inMobiNative);
        }
    }


}
