package com.gradivusmedia.yuliapp.view.maps;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.gradivusmedia.yuliapp.events.LocationChangeEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;

import org.greenrobot.eventbus.EventBus;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;

import javax.inject.Inject;

public class CustomGpsMyLocationProvider extends GpsMyLocationProvider {

    @Inject
    EventBus bus;

    public CustomGpsMyLocationProvider(Context context) {
        super(context);
        DI.get().inject(this);
    }

    @Override
    public void onLocationChanged( Location location) {
        super.onLocationChanged(location);
        Log.v("Location", location.toString());
        bus.post(new LocationChangeEvent(LocationChangeEvent.LOCATION_OK , location));
    }
}
