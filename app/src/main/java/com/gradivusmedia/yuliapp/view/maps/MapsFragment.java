package com.gradivusmedia.yuliapp.view.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gradivusmedia.yuliapp.BuildConfig;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.events.LocationChangeEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.Establecimiento;
import com.gradivusmedia.yuliapp.utils.CustomInfoWindow;
import com.gradivusmedia.yuliapp.view.adapters.rvEstablecimientosAdapter;

import android.support.v4.app.Fragment;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MapsFragment extends Fragment {

    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    Context ctx;

    @BindView(R.id.map)
    MapView map;

    @BindView(R.id.txttitulo)
    TextView txttitulo;

    //Maps Location variables
    MyLocationNewOverlay mLocationOverlay;
    //Compass overlay
    CompassOverlay mCompassOverlay;

    private List<Establecimiento> establecimientoslist;

    //El que manejara la ubicación del usuario
    CustomGpsMyLocationProvider myProvider;

        //Static factory method
        public static MapsFragment newInstance(Bundle args){
            MapsFragment frag = new MapsFragment();
            frag.setArguments(args);
            return frag;
        }

    private static MapsFragment instancia = null;
    public static MapsFragment getInstance(){
        if(instancia == null){
            instancia = new MapsFragment();
            //frag.setArguments(args);
        }
        return  instancia;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
       // map = view.findViewById(R.id.map);

        //Bind view
        ButterKnife.bind(this,view);

        ctx = getContext();

        myProvider = new CustomGpsMyLocationProvider(ctx);

        //Init params on created
        onViewCreated();

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_establecimientos, container, false);
        return view;
    }

    public void initMap(){
        //Inicializar el tipo de TileSource del mapa
        map.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(13);
        GeoPoint startPoint = new GeoPoint(12.0978181, -86.3285017);
        mapController.setCenter(startPoint);

        //Para prevenir ser banneado por abusar del tiles
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);

        map.getOverlayManager().getTilesOverlay().setLoadingBackgroundColor(R.color.colorPrimaryDark);
        map.getOverlayManager().getTilesOverlay().setLoadingLineColor(R.color.colorPrimary);

        //Get The Overlays icons
        Bitmap icon = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.round_my_location_black_24dp);

        //Add overlay location
        this.mLocationOverlay = new MyLocationNewOverlay(myProvider, map);
        this.mLocationOverlay.enableMyLocation();
        this.mLocationOverlay.setPersonIcon(icon);
        map.getOverlays().add(this.mLocationOverlay);


        //Agregar compass overlay
        this.mCompassOverlay = new CompassOverlay(ctx, new InternalCompassOrientationProvider(ctx), map);
        this.mCompassOverlay.enableCompass();
        map.getOverlays().add(this.mCompassOverlay);

        //Agrear latitud longitud gridlines Only for debug purposes
        if(MConfig.getIsDebug()){
            //LatLonGridlineOverlay2 overlay = new LatLonGridlineOverlay2();
            //map.getOverlays().add(overlay);
        }

        setItemIcons();

    }


    public void setItemIcons(){

        //Limpiamos todos los Overlays si hubieran
        map.getOverlays().clear();

        //Consultar la lista de BD objectbox
        DBService service_database = new DBService();
        establecimientoslist =   service_database.getEstablecimientosBD();

        //your items   12.123739, -86.259832
        ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();


        //------------------------------------
        FolderOverlay poiMarkers = new FolderOverlay(ctx);
        map.getOverlays().add(poiMarkers);
        Drawable poiIcon = getResources().getDrawable(R.drawable.ic_pin_marker);

        for (Establecimiento item: establecimientoslist) {

            double lLat = Double.parseDouble(item.getLat());
            double lLon = Double.parseDouble(item.getLon());

            Marker poiMarker = new Marker(map);
            poiMarker.setTitle( item.getNombre() );
            poiMarker.setSnippet( item.getDescripcion() );
            //poiMarker.setPosition( new GeoPoint(12.116568d,-86.261457d) );
            poiMarker.setPosition( new GeoPoint( lLat, lLon ) );
            poiMarker.setIcon(poiIcon);
            if ( item.getImagen() != null){
                Drawable draw = getResources().getDrawable(R.drawable.round_pageview_white_36dp);
                draw.setTint(getResources().getColor(R.color.colorPrimary));
                poiMarker.setImage( draw ); //  .setImage(new BitmapDrawable(poi.mThumbnail));
            }

            CustomInfoWindow infobubble = new CustomInfoWindow(map, ctx);
            infobubble.setEstab(item);

            poiMarker.setInfoWindow(infobubble);
            poiMarkers.add(poiMarker);


            /*
            Drawable newMarker = this.getResources().getDrawable(R.drawable.ic_pin_location);
            OverlayItem olItem = new OverlayItem("" + item.getNombre(),
                    " " + item.getDescripcion(),
                    new GeoPoint(
                            Double.parseDouble(item.getLat()),
                            Double.parseDouble(item.getLon())
                    ));
            olItem.setMarker(newMarker);
            items.add(olItem); */

        }

/*        Drawable newMarker = this.getResources().getDrawable(R.drawable.ic_pin_location);
        OverlayItem olItem = new OverlayItem("Title", "Description", new GeoPoint(12.116568d,-86.261457d));
        olItem.setMarker(newMarker);
        items.add(olItem);


        newMarker = this.getResources().getDrawable(R.drawable.ic_pin_location_blue);
        olItem = new OverlayItem("Title 2", "Description", new GeoPoint(12.123739d,-86.259832d));
        olItem.setMarker(newMarker);
        items.add(olItem);

        */

        //the overlay
        ItemizedOverlayWithFocus<OverlayItem> mOverlay = new ItemizedOverlayWithFocus<OverlayItem>(ctx,
                items, new OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int index, OverlayItem item) {
                return false;
            }

            @Override
            public boolean onItemLongPress(int index, OverlayItem item) {
                return false;
            }
        }
        );
        mOverlay.setFocusItemsOnTap(true);

        map.getOverlays().add(mOverlay);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
        }catch (Exception ex){
            Log.e("onResume", ex.toString());
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        try{
            map.onPause(); //needed for compass, my location overlays, v6.0.0 and up
        }catch (Exception ex){
            Log.e("onPause", ex.toString());
        }
    }

    public void onViewCreated(){

        //Inject dependencies
        initInjector();

        //Register event bus
        bus.register(this);

        //Inicializar las funciones del mapa
         initMap();

        //Get events
        //initEstablecimientosList();
    }

    // Called when the view is destroyed, i normally use this method to unbind view, and
    // unregister the event bus
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            if (bus.isRegistered(this)) bus.unregister(this);
        }catch (Exception ex){

        }
    }

    // Inject the fragment
    private void initInjector(){
        DI.get().inject(this);
    }


    private void initEstablecimientosList(){

        try{
            DBService service_database = new DBService();
            establecimientoslist =   service_database.getEstablecimientosBD();
            //establecimientoslist = new ArrayList<>();
            //Crear el Adapter
            //mAdapter = new rvEstablecimientosAdapter(ctx, establecimientoslist );
            //list.setAdapter(mAdapter);

            if(establecimientoslist != null){
                if(establecimientoslist.size() > 0){
                //    rlempty.setVisibility(View.GONE);
                //    list.setVisibility(View.VISIBLE);
                }else {
                //    rlempty.setVisibility(View.VISIBLE);
                //   list.setVisibility(View.GONE);
                }
            }else{
            //    rlempty.setVisibility(View.VISIBLE);
            //   list.setVisibility(View.GONE);
            }

        }catch (Exception ex){
            Log.e("initEstat", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();

           // rlempty.setVisibility(View.GONE);
           // list.setVisibility(View.VISIBLE);
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(LocationChangeEvent e){
        try{
            Log.v("onEventMainThread","Received the events");

            Location ubicacion = e.getLocation();
            IMapController mapController = map.getController();
            GeoPoint startPoint = new GeoPoint(ubicacion.getLatitude(), ubicacion.getLongitude());
            mapController.setCenter(startPoint);

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }

    }

}
