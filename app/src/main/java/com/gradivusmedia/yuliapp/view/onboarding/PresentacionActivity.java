package com.gradivusmedia.yuliapp.view.onboarding;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.gradivusmedia.yuliapp.BuildConfig;
import com.gradivusmedia.yuliapp.MainApplication;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.config.MConfig;
import com.gradivusmedia.yuliapp.utils.Funciones;
import com.gradivusmedia.yuliapp.view.MainActivity;

public class PresentacionActivity extends AppCompatActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    ImageButton mNextBtn;
    Button mSkipBtn, mFinishBtn;

    ImageView zero, one, two;
    ImageView[] indicators;

    int lastLeftValue = 0;
    CoordinatorLayout mCoordinator;
    static final String TAG = "PagerActivity";
    int page = 0;   //  to track page position


    // Remote Config keys
    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";



    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    public static String welcomeMessage1 = "";
    public static String welcomeMessage2 = "";
    public static String welcomeMessage3 = "";

    public static String tituloMessage1 = "";
    public static String tituloMessage2 = "";
    public static String tituloMessage3 = "";

    public static String colorMessage1 = "";
    public static String colorMessage2 = "";
    public static String colorMessage3 = "";


    public static String welcome_image_1 = "";
    public static String welcome_image_2 = "";
    public static String welcome_image_3 = "";


    int color1 ;
    int color2 ;
    int color3 ;
    int[] colorList = new int[]{color1, color2, color3};

    static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        setContentView(R.layout.activity_presentacion);

         color1 = ContextCompat.getColor(this, R.color.color1);
         color2 = ContextCompat.getColor(this, R.color.color2);
         color3 = ContextCompat.getColor(this, R.color.color3);

         ctx = PresentacionActivity.this;

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mNextBtn = (ImageButton) findViewById(R.id.intro_btn_next);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
            mNextBtn.setImageDrawable(
                    Funciones.tintDrawable(ContextCompat.getDrawable(this, R.drawable.round_chevron_right_black_24dp), Color.WHITE)
            );

        mSkipBtn = (Button) findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) findViewById(R.id.intro_btn_finish);

        zero = (ImageView) findViewById(R.id.intro_indicator_0);
        one = (ImageView) findViewById(R.id.intro_indicator_1);
        two = (ImageView) findViewById(R.id.intro_indicator_2);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.main_content);

        indicators = new ImageView[]{zero, one, two};

        //----------------------------------------------------
        // Get Remote Config instance.
        // [START get_remote_config_instance]
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        // [END get_remote_config_instance]

        // Create a Remote Config Setting to enable developer mode, which you can use to increase
        // the number of fetches available per hour during development. See Best Practices in the
        // README for more information.
        // [START enable_dev_mode]
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        // [END enable_dev_mode]


        // Set default Remote Config parameter values. An app uses the in-app default values, and
        // when you need to adjust those defaults, you set an updated value for only the values you
        // want to change in the Firebase console. See Best Practices in the README for more
        // information.
        // [START set_default_values]
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        // [END set_default_values]

        fetchConfig();
        //----------------------------------------------------
    }

    private void InitWelcomeAdapter(){

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setCurrentItem(page);
        updateIndicators(page);

        try{
            color1 = Color.parseColor(colorMessage1.trim()); // ContextCompat.getColor(this, R.color.color1);
            color2 = Color.parseColor(colorMessage2.trim()); //ContextCompat.getColor(this, R.color.color2);
            color3 = Color.parseColor(colorMessage3.trim()); //ContextCompat.getColor(this, R.color.color3);
        }catch (Exception ex){
            Log.e("erroColor", ex.toString());
        }

        //final int[] colorList = new int[]{color1, color2, color3};
        colorList = new int[]{color1, color2, color3};

        final ArgbEvaluator evaluator = new ArgbEvaluator();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                /*
                color update
                 */
                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 2 ? position : position + 1]);
                mViewPager.setBackgroundColor(colorUpdate);

            }

            @Override
            public void onPageSelected(int position) {

                page = position;

                updateIndicators(page);

                switch (position) {
                    case 0:
                        mViewPager.setBackgroundColor(color1);
                        break;
                    case 1:
                        mViewPager.setBackgroundColor(color2);
                        break;
                    case 2:
                        mViewPager.setBackgroundColor(color3);
                        break;
                }


                mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                mViewPager.setCurrentItem(page, true);
            }
        });

        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Funciones.saveSharedSetting(PresentacionActivity.this, MainActivity.PREF_USER_FIRST_TIME, "false");
            }
        });


    }



    private void fetchConfig() {
        Log.v("LOADING", mFirebaseRemoteConfig.getString(LOADING_PHRASE_CONFIG_KEY));
        long cacheExpiration = 3600; // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                           /* Toast.makeText(MainActivity.this, "Fetch Succeeded",
                                    Toast.LENGTH_SHORT).show(); */
                            Log.v("success Fetch", "Fetch success");

                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        } else {
                            /// Toast.makeText(MainActivity.this, "Fetch Failed", Toast.LENGTH_SHORT).show();
                            Log.v("Failed Fetch", "Fetch Failed");

                        }
                        displayWelcomeMessage();
                    }
                });
        // [END fetch_config_with_callback]
    }


    private void displayWelcomeMessage() {
        // [START get_config_values]
         welcomeMessage1 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_MESSAGE_PAGINA1 );
         welcomeMessage2 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_MESSAGE_PAGINA2);
         welcomeMessage3 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_MESSAGE_PAGINA3);

        colorMessage1 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_COLOR_PAGINA1) ;
        colorMessage2 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_COLOR_PAGINA2) ;
        colorMessage3 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_COLOR_PAGINA3) ;

        tituloMessage1 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_TITLE_PAGINA1) ;
        tituloMessage2 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_TITLE_PAGINA2) ;
        tituloMessage3 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_TITLE_PAGINA3) ;

        welcome_image_1 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_IMAGEN_PAGINA1) ;
        welcome_image_2 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_IMAGEN_PAGINA2) ;
        welcome_image_3 = mFirebaseRemoteConfig.getString(MConfig.WELCOME_IMAGEN_PAGINA3) ;
        // [END get_config_values]

        // mWelcomeTextView.setText(welcomeMessage);
        InitWelcomeAdapter();
    }



    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_presentacion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * responsible for each slide’s layout
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        ImageView img;
        int[] bgs = new int[]{R.drawable.round_flight_black_24dp,
                R.drawable.round_email_black_24dp,
                R.drawable.round_explore_black_24dp};

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_presentacion, container, false);
            TextView txtTitulo = (TextView) rootView.findViewById(R.id.txt_titulo);
            TextView textViewContenido = (TextView) rootView.findViewById(R.id.txt_contenido);
            txtTitulo.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            img = (ImageView) rootView.findViewById(R.id.section_img);


            switch (getArguments().getInt(ARG_SECTION_NUMBER) ){
                    case 1:
                        txtTitulo.setText(tituloMessage1);
                        textViewContenido.setText(welcomeMessage1);
                        //img.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);

                        Glide.with(ctx)
                                .load(welcome_image_1)
                                .into(img);
                        break;
                    case 2:
                        txtTitulo.setText(tituloMessage2);
                        textViewContenido.setText(welcomeMessage2);
                        img.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);

                        Glide.with(ctx)
                                .load(welcome_image_2)
                                .into(img);
                        break;
                    case 3:
                        txtTitulo.setText(tituloMessage3);
                        textViewContenido.setText(welcomeMessage3);
                        img.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);

                        Glide.with(ctx)
                                .load(welcome_image_3)
                                .into(img);
                        break;
                    default: textViewContenido.setText("Default text");
            }
           // String texto = Funciones.readSharedSetting(MainApplication.getContext(), "WelcomeText", "Default");
           // textView.setText(texto);

            ///img.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
            return rootView;
        }
    }

    /**
     * ViewPager Adapter. Handles the slides
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
