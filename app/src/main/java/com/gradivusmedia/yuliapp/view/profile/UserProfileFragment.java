package com.gradivusmedia.yuliapp.view.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.gradivusmedia.yuliapp.R;
import com.gradivusmedia.yuliapp.controller.MainController;
import com.gradivusmedia.yuliapp.events.GetEstablecimientosFinishedEvent;
import com.gradivusmedia.yuliapp.internal.di.DI;
import com.gradivusmedia.yuliapp.model.db.DBService;
import com.gradivusmedia.yuliapp.model.pojos.Usuario;
import com.gradivusmedia.yuliapp.utils.Funciones;
import com.gradivusmedia.yuliapp.view.about.AboutActivity;
import com.gradivusmedia.yuliapp.view.about.TerminosActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends Fragment {

    @Inject
    MainController controller;

    @Inject
    EventBus bus;

    Context ctx;

    //-------------------------------
    @BindString(R.string.str_perfil_usuario) String str_perfil_usuario;

    @BindString(R.string.str_cerrar_salir_app_confirmar) String dialogCerrarSesion;

    @BindString(R.string.str_yes) String strYes;

    @BindString(R.string.str_no) String strNo;

    @BindString(R.string.app_name) String strAppName;

    //-------------------------------
    @BindView(R.id.txttitulo)
    TextView txttitulo;

    @BindView(R.id.btn_acerca_de)
    Button btn_acerca_de;

    @BindView(R.id.btn_terminos_condiciones)
    Button btn_terminos_condiciones;

    @BindView(R.id.btn_aceptar)
    Button btn_aceptar;

    @BindView(R.id.btn_aceptar_iniciar_sesion)
    Button btn_aceptar_iniciar_sesion;

    @BindView(R.id.btn_cerrar_sesion)
    Button btn_cerrar_sesion;

    @BindView(R.id.txt_nombre)
    EditText txt_nombre;

    @BindView(R.id.txt_email)
    EditText txt_email;

    @BindView(R.id.txt_telefono)
    EditText txt_telefono;

    @BindView(R.id.img_profile_picture)
    ImageView img_profile_picture;

    //---------------------------
    @BindView(R.id.llregistro)
    LinearLayout llregistro;

    @BindView(R.id.llprofile)
    LinearLayout llprofile;

    @BindView(R.id.llabout)
    LinearLayout llabout;


    private static final int RC_SIGN_IN = 123;

    FirebaseUser user;
    FirebaseAuth auth;

    // Choose authentication providers
    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            // new AuthUI.IdpConfig.PhoneBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build(),
            new AuthUI.IdpConfig.FacebookBuilder().build()
            // new AuthUI.IdpConfig.TwitterBuilder().build()
    );


    //Static factory method
    public static UserProfileFragment newInstance(Bundle args){
        UserProfileFragment frag = new UserProfileFragment();
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        //Bind view
        ButterKnife.bind(this,view);

        ctx = getContext();

        onViewCreated();

        txttitulo.setText("" + str_perfil_usuario );


        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            // User is signed in (getCurrentUser() will be null if not signed in)
            user = FirebaseAuth.getInstance().getCurrentUser();
            initUserData();
            llregistro.setVisibility(View.GONE);
            llprofile.setVisibility(View.VISIBLE);
        }else{
            llregistro.setVisibility(View.VISIBLE);
            llprofile.setVisibility(View.GONE);
        }


        return view; //super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    public void onViewCreated(){

        //Inject dependencies
        initInjector();

        //Register event bus
        bus.register(this);

        //Init events list
        ///  initEstablecimientosList();

        //Get events
       ///// getEvents();
    }

    private void initInjector(){
        DI.get().inject(this);
    }



    @OnClick(R.id.btn_cerrar_sesion)
    public void clickCerrarSesion() {

        new AlertDialog.Builder(ctx)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("" + strAppName)
                .setMessage("" + dialogCerrarSesion)
                .setPositiveButton("" + strYes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            AuthUI.getInstance()
                                    .signOut(ctx)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        public void onComplete(@NonNull Task<Void> task) {
                                            // ...
                                            llprofile.setVisibility(View.GONE);
                                            llregistro.setVisibility(View.VISIBLE);

                                            deleteAllUsers();
                                        }
                                    });
                        }catch (Exception ex){
                            Log.e("clickCerrarSesion", ex.toString());
                        }
                    }

                })
                .setNegativeButton("" + strNo, null)
                .show();
    }


    @OnClick(R.id.btn_aceptar_iniciar_sesion)
    public void clickIniciarSesion() {
        // View Terminos
        try{

            // Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(),
                    RC_SIGN_IN);

        }catch (Exception ex){
            Log.e("clickIniciarSesion", ex.toString());
        }
    }


    private void deleteAllUsers(){
        try{
            DBService service_database = new DBService();
            if(service_database.deleteAllUsers()){
                Log.v("DT", "Deleted all data");
            }else{
                Log.v("DT", "No se pudo borrar");
            }

        }catch (Exception ex){

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                user = FirebaseAuth.getInstance().getCurrentUser();

                initUserData();

                llprofile.setVisibility(View.VISIBLE);
                llregistro.setVisibility(View.GONE);
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }


    private void initUserData(){
        try{
            txt_nombre.setText( user.getDisplayName() );
            txt_email.setText( user.getEmail() );
            txt_telefono.setText( user.getPhoneNumber() );
            Glide.with(ctx)
                    .load( user.getPhotoUrl() )
                    .apply(RequestOptions.circleCropTransform())
                    .into( img_profile_picture );

            Funciones.saveSharedSetting(ctx, "UID", user.getUid());

            Usuario appuser = new Usuario(1,
                    1,
                    user.getDisplayName(),
                    user.getDisplayName(),
                    user.getPhotoUrl().toString(),
                    user.getEmail(),
                    1,
                    "",
                    "",
                    "");

            controller.sendUserProfile( appuser, 1, user.getUid() );

        }catch (Exception ex){
            Log.e("setUserProfile", ex.toString());
        }
    }


    @OnClick(R.id.btn_terminos_condiciones)
    public void clickTerminos() {
        // View Terminos
        try{
            Intent mIntent = new Intent(ctx, TerminosActivity.class);
            //Bundle mBundle = new Bundle();
            //mIntent.putExtra("establecimiento", mData.get(position)); // enviamos el objeto serializado
            //mIntent.putExtras(mBundle);
            ctx.startActivity(mIntent);
        }catch (Exception ex){
            Log.e("clickTerminos", ex.toString());
        }
    }

    @OnClick(R.id.btn_acerca_de)
    public void clickAcercaDe() {
        // View About
        try{
            Intent mIntent = new Intent(ctx, AboutActivity.class);
            //Bundle mBundle = new Bundle();
            //mIntent.putExtra("establecimiento", mData.get(position)); // enviamos el objeto serializado
            //mIntent.putExtras(mBundle);
            ctx.startActivity(mIntent);
        }catch (Exception ex){
            Log.e("clickAcercaDe", ex.toString());
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try{
            if (bus.isRegistered(this)) bus.unregister(this);
        }catch (Exception ex){

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(GetEstablecimientosFinishedEvent e){
        try{
            Log.v("onEventMainThread","Received the events");

        }catch (Exception ex){
            Log.e("onEventMainThread", ex.toString());
            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
        }

    }


}
